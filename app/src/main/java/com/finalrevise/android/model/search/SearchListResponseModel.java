package com.finalrevise.android.model.search;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchListResponseModel {

    private Integer status;
    private Integer statusCode;
    private String msg;
    @SerializedName("data")
    private List<Result> result = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

}
