package com.finalrevise.android.model.material

data class LinkedPageItemModel (
    var pageId: Long?,
    var pageItemName: String?,
    var pageDocLink: String?){
}