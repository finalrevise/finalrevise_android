package com.finalrevise.android.model.subcat

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.HashMap

class GroupedPageItemModel {

    @SerializedName("pageId")
    @Expose
    private var pageId: Long? = null
    @SerializedName("pageItemName")
    @Expose
    private var pageItemName: String? = null
    @SerializedName("total")
    @Expose
    private var total: Int? = null
    @SerializedName("isChild")
    @Expose
    private var isChild: Boolean? = null
    @SerializedName("tag")
    @Expose
    private var tag: String? = null
    @SerializedName("groupedPageItems")
    private var result: HashMap<String, List<GroupedPageItemModel>>? = null
    @SerializedName("coverUrl")
    @Expose
    private var coverUrl: String? = null
    @SerializedName("metaTitle")
    @Expose
    private var metaTitle: String? = null

    fun getResult(): HashMap<String, List<GroupedPageItemModel>>? {
        return result
    }

    fun setResult(result: HashMap<String, List<GroupedPageItemModel>>) {
        this.result = result
    }

    fun getPageItemName(): String? {
        return pageItemName
    }

    fun setPageItemName(pageItemName: String) {
        this.pageItemName = pageItemName
    }

    fun getTotal(): Int? {
        return total
    }

    fun setTotal(total: Int?) {
        this.total = total
    }

    fun getIsChild(): Boolean? {
        return isChild
    }

    fun setIsChild(isChild: Boolean?) {
        this.isChild = isChild
    }

    fun getTag(): String? {
        return tag
    }

    fun setTag(tag: String) {
        this.tag = tag
    }

    fun getPageId(): Long? {
        return pageId
    }

    fun setPageId(pageId: Long?) {
        this.pageId = pageId
    }

    fun getCoverUrl(): String? {
        return coverUrl
    }

    fun setCoverUrl(coverUrl: String) {
        this.coverUrl = coverUrl
    }

    fun getMetaTitle(): String? {
        return metaTitle
    }

    fun setMetaTitle(metaTitle: String) {
        this.metaTitle = metaTitle
    }
}