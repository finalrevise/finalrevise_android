package com.finalrevise.android.model.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Entity {

    @SerializedName("data")
    @Expose
    var data: List<Data>? = null
    @SerializedName("metaKeyword")
    @Expose
    var metaKeyword: String? = null
    @SerializedName("metaDesc")
    @Expose
    var metaDesc: String? = null
    @SerializedName("tag")
    @Expose
    var tag: String? = null

}