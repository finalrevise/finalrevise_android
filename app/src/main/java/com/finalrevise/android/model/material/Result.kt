package com.finalrevise.android.model.material

import androidx.lifecycle.MutableLiveData
import com.google.gson.annotations.SerializedName
import java.util.*

class Result : MutableLiveData<Any?>() {
    var pageTitle: String? = null
    var description: String? = null
    var pagePath: String? = null

    @SerializedName("linkedPageItems")
    var linkedPageItems: ArrayList<LinkedPageItemModel>? = null
    var tag: String? = null
}