package com.finalrevise.android.model.material

import com.google.gson.annotations.SerializedName

data class DetailResponseModel(
    var status: Int,
    var statusCode: Int,
    var msg: String,
    @SerializedName("data")
    var result: Result
)