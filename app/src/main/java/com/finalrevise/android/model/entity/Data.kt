package com.finalrevise.android.model.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Data {

    @SerializedName("entityName")
    @Expose
    var entityName: String? = null
    @SerializedName("entityImage")
    @Expose
    var entityImage: String? = null
    @SerializedName("catId")
    @Expose
    var catId: Int? = null
    @SerializedName("catName")
    @Expose
    var catName: String? = null
    @SerializedName("tag")
    @Expose
    var tag: String? = null

}
