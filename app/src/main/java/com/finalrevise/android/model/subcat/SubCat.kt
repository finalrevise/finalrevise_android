package com.finalrevise.android.model.subcat

import com.google.gson.annotations.SerializedName

class SubCat {

    private var status: Int? = null
    private var statusCode: Int? = null
    private var msg: String? = null
    @SerializedName("data")
    private var result: Result? = null

    fun getStatus(): Int? {
        return status
    }

    fun setStatus(status: Int?) {
        this.status = status
    }

    fun getStatusCode(): Int? {
        return statusCode
    }

    fun setStatusCode(statusCode: Int?) {
        this.statusCode = statusCode
    }

    fun getMsg(): String? {
        return msg
    }

    fun setMsg(msg: String) {
        this.msg = msg
    }

    fun getResult(): Result? {
        return result
    }

    fun setResult(result: Result) {
        this.result = result
    }

}
