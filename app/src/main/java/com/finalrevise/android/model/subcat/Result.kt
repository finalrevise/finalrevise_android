package com.finalrevise.android.model.subcat

import com.google.gson.annotations.SerializedName
import java.util.HashMap

class Result {
    private var pageTitle: String? = null
    @SerializedName("groupedPageItems")
    private var result: HashMap<String, List<GroupedPageItemModel>>? = null

    fun getPageTitle(): String? {
        return pageTitle
    }

    fun setPageTitle(pageTitle: String) {
        this.pageTitle = pageTitle
    }

    fun getResult(): HashMap<String, List<GroupedPageItemModel>>? {
        return result
    }

    fun setResult(result: HashMap<String, List<GroupedPageItemModel>>) {
        this.result = result
    }
}
