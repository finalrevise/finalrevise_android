package com.finalrevise.android.base

import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentTransaction
import com.finalrevise.android.R
import com.finalrevise.android.interfaces.IDrawerLocker
import com.finalrevise.android.utils.CommonUtils
import com.finalrevise.android.utils.Logger
import com.google.android.material.internal.ScrimInsetsFrameLayout


abstract class BaseDrawerActivity : BaseContentActivity(), IDrawerLocker {

    private var mDrawerLayout: DrawerLayout? = null
    private var mLeftDrawer: ScrimInsetsFrameLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Setup drawer layout
        initLeftDrawerMenu()
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_with_drawer
    }

    private fun initLeftDrawerMenu() {
        mDrawerLayout = findViewById(R.id.drawer_layout)

        if (mDrawerLayout == null) {
            return
        }

        val transaction = supportFragmentManager.beginTransaction()
        transaction.setTransition(FragmentTransaction.TRANSIT_NONE)
        transaction.replace(R.id.left_drawer_parent, getDrawerFragment())
        transaction.commit()

        mLeftDrawer = findViewById(R.id.left_drawer_parent)
        mDrawerLayout!!.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START)
    }

    override fun onNavigationIconClick(showHamburger: Boolean) {
        try {
            if (showHamburger) {
                mDrawerLayout?.openDrawer(GravityCompat.START)
            } else {
                super.onNavigationIconClick(showHamburger)
            }
        } catch (e: Exception) {
            Logger.e("Error while handling navigation click - " + CommonUtils.getClassAlias(this), e)
        }

    }

    private fun closeDrawer(): Boolean {
        if (mDrawerLayout != null && mLeftDrawer != null && mDrawerLayout!!.isDrawerOpen(
                mLeftDrawer!!
            )
        ) {
            mDrawerLayout!!.closeDrawer(GravityCompat.START)
            return false
        }
        return true
    }

    fun closeDrawerOnItemClick() {
        closeDrawer()
    }

    override fun onBackPressed() {
        if (closeDrawer()) {
            super.onBackPressed()
        }
    }

    override fun setDrawerLocked(shouldLock: Boolean) {
        if (mDrawerLayout != null) {
            val lockMode = if (shouldLock) DrawerLayout.LOCK_MODE_LOCKED_CLOSED else DrawerLayout.LOCK_MODE_UNLOCKED
            mDrawerLayout!!.setDrawerLockMode(lockMode)
        }
    }

    abstract fun getDrawerFragment(): BaseFragment

}

