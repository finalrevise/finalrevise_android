package com.finalrevise.android.base

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.finalrevise.android.FinalReviseApplication
import com.finalrevise.android.R
import com.finalrevise.android.utils.AppLoader
import com.finalrevise.android.utils.CommonUtils
import com.finalrevise.android.utils.Logger
import com.finalrevise.android.view.subcat.SubCatFragment
import com.google.android.gms.ads.*
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback



abstract class BaseFragment : DialogFragment(), Toolbar.OnMenuItemClickListener {

    lateinit var mContext: Context
    private var showNavigationIcon = true
    @JvmField
    var mainActivity: AppCompatActivity? = null
    private lateinit var menuInflater: MenuInflater
    private var navigationIcon = 0
    private var showHamburgerMenu = false
    private var showLogo = false
    private var toolbarTitle = ""
    private var primeProgressLoader: AppLoader? = null
    @JvmField
    var mAdView: AdView? = null
    var mInterstitialAd: InterstitialAd? = null

    fun isShowNavigationIcon(): Boolean {
        return showNavigationIcon
    }

    fun setShowNavigationIcon(showNavigationIcon: Boolean) {
        this.showNavigationIcon = showNavigationIcon
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        this.mainActivity = activity as AppCompatActivity
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context != null && context is AppCompatActivity) {
            this.mainActivity = context
            this.mContext = context
        }
    }

    companion object {
        fun replaceToBackStack(
            manager: FragmentManager,
            targetId: Int,
            fragment: BaseFragment?,
            isAddToBackStace: Boolean
        ) {
            if (fragment != null) {
                val mTag = CommonUtils.getClassAlias(fragment)

                try {
                    val transaction = manager.beginTransaction()
                    if (isAddToBackStace)
                        transaction.addToBackStack(null)
                    else {
                        transaction.setTransition(FragmentTransaction.TRANSIT_NONE)
                        transaction.add(targetId, fragment, mTag)
                        transaction.addToBackStack(mTag)
                    }
                    transaction.commitAllowingStateLoss()
                } catch (e: Exception) {
                    Logger.e("Exception during replacing fragment for $mTag", e)
                }

            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.menuInflater = MenuInflater(activity)
        MobileAds.initialize(mContext) {}
    }

    private fun createInterstitialAd(adRequest: AdRequest) {
        context?.let {
            InterstitialAd.load(
                it,
                getString(R.string.interstitial_full_screen),
                adRequest,
                object : InterstitialAdLoadCallback() {
                    override fun onAdLoaded(ad: InterstitialAd) {
                        mInterstitialAd = ad
                        mInterstitialAd?.fullScreenContentCallback = fullScreenContentCallback
                    }

                })
        }
    }

    // Create a full screen content callback.
    private val fullScreenContentCallback = object : FullScreenContentCallback() {

        override fun onAdDismissedFullScreenContent() {
            super.onAdDismissedFullScreenContent()
            mInterstitialAd = null
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        invalidateOptionMenu()
        initializeToolBar()
        val adRequest = AdRequest.Builder()
//            .addTestDevice("199A6D3CEE0567397ABA208B977D5B8D")
            .build()
        createInterstitialAd(adRequest)
        if (mAdView != null) {
            mAdView!!.loadAd(adRequest)
        }
    }

    /**
     * Tool bar properties *
     */
    private fun initializeToolBar() {
        if (isToolbarExist()) {
            changeNavigationIcon()
            changeLogo()
            changeTitle()
        }
    }

    private fun changeNavigationIcon() {
        if (isToolbarExist()) {
            val toolbar = getToolbar()
            val navigationIcon = getNavigationIcon()
            if (!isShowNavigationIcon()) {
                // Do nothing
            } else if (isShowHamburgerMenu()) {
                toolbar!!.setNavigationIcon(R.drawable.ic_menu_hamburger)
                attachNavigationIconListener(toolbar)
            } else if (navigationIcon != 0) {
                toolbar!!.setNavigationIcon(navigationIcon)
                attachNavigationIconListener(toolbar)
            }
        }
    }

    var toolbarNavigationClickListener: View.OnClickListener = View.OnClickListener {
        if (confirmToolbarNavigation()) {
            executeToolbarNavigation()
        }
    }

    /**
     * Callback for toolbar back navigation click for child fragments to handle.
     * If you override then please call
     *
     * @return
     */
    private fun confirmToolbarNavigation(): Boolean {
        return true
    }

    private fun executeToolbarNavigation() {
        if (mainActivity is BaseContentActivity) {
            (mainActivity as BaseContentActivity).onNavigationIconClick(showHamburgerMenu)
        } else if (mainActivity is BaseDrawerActivity) {
            (mainActivity as BaseDrawerActivity).onNavigationIconClick(showHamburgerMenu)
        }
    }

    fun attachNavigationIconListener(toolbar: Toolbar) {
        toolbar.setNavigationOnClickListener(toolbarNavigationClickListener)
    }

    fun getNavigationIcon(): Int {
        return if (navigationIcon != 0) navigationIcon else R.drawable.ic_menu_back
    }

    fun isShowHamburgerMenu(): Boolean {
        return this.showHamburgerMenu
    }

    /**
     * Menu handling create option again.
     */
    protected fun invalidateOptionMenu() {
        if (isToolbarExist()) {
            val toolbar = getToolbar()
            toolbar!!.menu.clear()
            toolbar.setOnMenuItemClickListener(this)
            onCreateOptionsMenu(toolbar.menu, menuInflater)
            onPrepareOptionsMenu(toolbar.menu)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        val menuResourceId = getToolbarMenu()
        if (menuResourceId != 0) {
            inflater.inflate(menuResourceId, menu)
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    fun isToolbarExist(): Boolean {
        val mView = view
        val toolbar = mView?.findViewById<View>(R.id.toolBar)
        return toolbar != null
    }

    fun getToolbar(): Toolbar? {
        if (isToolbarExist()) {
            val mView = view
            return mView!!.findViewById<View>(R.id.toolBar) as Toolbar
        }
        return null
    }

    private fun changeLogo() {
        if (isToolbarExist() && isShowLogo()) {
            val appToolbarLogo = getLogo()
            if (appToolbarLogo != 0) getToolbar()?.logo = FinalReviseApplication.application?.let {
                ContextCompat.getDrawable(
                    it,
                    appToolbarLogo
                )
            }
        }
    }

    fun openDetailFragment(pageType: String, title: String) {
        val bundle = Bundle()
        val detailListFragment = SubCatFragment.newInstance()
        bundle.putString("type", pageType)
        bundle.putString("title", title)
        bundle.putBoolean("isSearch", true)
        detailListFragment.arguments = bundle
        replaceToBackStack(
            mainActivity?.supportFragmentManager!!,
            R.id.container,
            detailListFragment,
            false
        )
    }

    fun isShowLogo(): Boolean {
        return this.showLogo
    }

    // TODO
    // return app toolbar logo if requirement is defined for the same
    private fun getLogo(): Int {
        return 0
    }

    private fun changeTitle() {
        changeTitle(getToolbarTitle())
    }

    fun changeTitle(title: String) {
        if (isToolbarExist()) {
            if (CommonUtils.isValidText(title)) this.getToolbar()!!.title = title
        }
    }

    fun getToolbarTitle(): String {
        return toolbarTitle
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        return true
    }

    fun setShowHamburgerMenu(showHamburgerMenu: Boolean) {
        this.showHamburgerMenu = showHamburgerMenu
    }

    abstract fun getToolbarMenu(): Int

    // Generic onResume callback for fragment class
    fun onFragmentResume() {}


    fun showBlockingLoader() {
        if (primeProgressLoader == null) {
            primeProgressLoader =
                AppLoader.Builder.newInstance().withToken(this).isCancelable(false).build()
            primeProgressLoader?.show()
        }
    }

    fun showInterstitial() {
        activity?.let { mInterstitialAd?.show(it) }
    }

    fun showBlockingLoader(isCancel: Boolean) {
        if (primeProgressLoader == null) {
            primeProgressLoader =
                AppLoader.Builder.newInstance().withToken(this).isCancelable(isCancel).build()
            primeProgressLoader?.show()
        }
    }

    fun hideBlockingLoader() {
        if (primeProgressLoader != null) {
            primeProgressLoader?.hide()
            primeProgressLoader = null
        }
    }

    // Method to show an alert dialog with yes, no and cancel button
    fun showAlertDialog(message: String, title: String) {
        // Late initialize an alert dialog object
        lateinit var dialog: AlertDialog


        // Initialize a new instance of alert dialog builder object
        val builder = AlertDialog.Builder(mContext)

        // Set a title for alert dialog
        builder.setTitle(title)

        // Set a message for alert dialog
        builder.setMessage(message)


        // On click listener for dialog buttons
        val dialogClickListener = DialogInterface.OnClickListener { _, which ->
            when (which) {
                DialogInterface.BUTTON_POSITIVE -> {
//                    val downloadStaticFragment = DownloadStaticFragment.newInstance()
//                    replaceToBackStack(
//                        mainActivity!!.supportFragmentManager,
//                        R.id.container,
//                        downloadStaticFragment,
//                        false
//                    )
                    dialog.dismiss()
                }
                DialogInterface.BUTTON_NEGATIVE -> dialog.dismiss()
            }
        }


        // Set the alert dialog positive/yes button
        builder.setPositiveButton("OK", dialogClickListener)

        // Set the alert dialog negative/no button
//        builder.setNegativeButton("Cancel", dialogClickListener)

        // Initialize the AlertDialog using builder object
        dialog = builder.create()

        // Finally, display the alert dialog
        dialog.show()
    }

    override fun onPause() {
        if (mAdView != null) {
            mAdView!!.pause()
        }
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        if (mAdView != null) {
            mAdView!!.resume()
        }
    }

    override fun onDestroy() {
        if (mAdView != null) {
            mAdView!!.destroy()
        }
        super.onDestroy()
    }
}
