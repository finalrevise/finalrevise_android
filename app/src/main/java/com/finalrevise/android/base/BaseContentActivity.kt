package com.finalrevise.android.base

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.finalrevise.android.R
import com.finalrevise.android.utils.CommonUtils
import com.finalrevise.android.utils.Logger

abstract class BaseContentActivity : AppCompatActivity() {

    lateinit var onBackStackChangedListener : FragmentManager.OnBackStackChangedListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setIntent(intent)
    }

    fun getFragmentAtPosition(fragmentManager: FragmentManager, index: Int) : BaseFragment? {
        if (index != -1) {
            val backStackEntry = fragmentManager.getBackStackEntryAt(index)
            val fragmentTag = backStackEntry.name

            if (!TextUtils.isEmpty(fragmentTag)) {
                val fragment = fragmentManager.findFragmentByTag(fragmentTag)
                if (fragment != null && fragment is BaseFragment) {
                    return fragment
                }
            }
        }
        return null
    }

    open fun onNavigationIconClick(showHamburger: Boolean) {
        try {
            supportFragmentManager.run {
                val shouldFinish = backStackEntryCount <= 1
                if (shouldFinish) {
                    finish()
                } else {
                    addOnBackStackChangedListener(getListener())
                    popBackStack()
                }
            }
        } catch (e: Exception) {
            Logger.e("Error while handling navigation click - " + CommonUtils.getClassAlias(this), e)
        }

    }

    /**
     * FragmentResume should be called only on pop of a fragment. Remove listener after
     * the Fragment resume is handled to prevent FragmentResume called again.
     * @return FragmentManager.OnBackStackChangedListener
     * @Author NitinA
     */
    private fun getListener(): FragmentManager.OnBackStackChangedListener {
        if (this::onBackStackChangedListener.isInitialized) {
            return onBackStackChangedListener
        }
        onBackStackChangedListener = FragmentManager.OnBackStackChangedListener {
            val manager = supportFragmentManager
            if (manager != null) {
                val currentFragment = manager.findFragmentById(R.id.container)
                if (currentFragment != null && currentFragment is BaseFragment) {
                    val primeFragment = currentFragment as BaseFragment?
                    primeFragment!!.onFragmentResume()
                }
                manager.removeOnBackStackChangedListener(onBackStackChangedListener)
            }
        }
        return onBackStackChangedListener
    }


    override fun onBackPressed() {
        try {
            val fragmentManager = supportFragmentManager
            if (fragmentManager != null) {
                val shouldFinish = fragmentManager.backStackEntryCount <= 1
                if (shouldFinish) {
                    finish()
                } else {
                    fragmentManager.addOnBackStackChangedListener(getListener())
                    fragmentManager.popBackStack()
                }
            }
        } catch (e: Exception) {
            Logger.e("Error while handling onBackPressed - " + CommonUtils.getClassAlias(this), e)
        }

    }

    abstract fun getLayoutId(): Int

}
