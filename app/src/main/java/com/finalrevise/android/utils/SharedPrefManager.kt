package com.finalrevise.android.utils

import android.content.Context
import android.content.SharedPreferences
import com.finalrevise.android.FinalReviseApplication
import java.util.*

class SharedPrefManager {
    val SHARED_PREF_FILE = "finalrevise_pref"

    companion object {
        private var mInstance: SharedPrefManager? = null

        fun getInstance(): SharedPrefManager? {
            if (mInstance == null) {
                synchronized(SharedPrefManager::class.java) {
                    if (mInstance == null) {
                        mInstance = SharedPrefManager()
                    }
                }
            }
            return mInstance
        }
    }

    private fun getSystemSharedPreference(): SharedPreferences? {
        val mContext = FinalReviseApplication.application
        return mContext?.getSharedPreferences(
            SHARED_PREF_FILE, Context.MODE_PRIVATE
        )
    }

    fun getSharedPreferenceEditor(): SharedPreferences.Editor? {
        val sharedPref = getSystemSharedPreference()
        return sharedPref?.edit()
    }

    private fun savePreference(editor: SharedPreferences.Editor?, key: String, value: Any) {
        if (editor != null) {
            if (value is String) {
                editor.putString(key, value.toString())
            } else if (value is Boolean) {
                editor.putBoolean(key, value)
            } else if (value is Int) {
                editor.putInt(key, value)
            }
        }
    }

    fun putPreference(key: String, value: Any?) {
        var value = value
        if (value == null) {
            value = ""
            //            throw new IllegalArgumentException("Shared Pref key/ value can't be NULL");
        }

        val editor = getSharedPreferenceEditor()

        try {
            savePreference(editor, key, value)
        } catch (e: Exception) {
            Logger.e(
                "Error saving shared pref -> @key - $key with Value - $value",
                e
            )
        } finally {
            editor?.apply()
        }
    }

    fun putMap(values: HashMap<String, Any>?) {
        if (values == null || values.size == 0) {
            throw IllegalArgumentException("Nothing to be saved.")
        }

        val editor = getSharedPreferenceEditor()

        try {
            val keys = values.keys
            for (key in keys) {
                values[key]?.let { savePreference(editor, key, it) }
            }
        } catch (e: Exception) {
            Logger.e("Error saving map in shared pref", e)
        } finally {
            editor?.apply()
        }
    }

    fun getString(key: String): String? {
        return getString(key, "")
    }

    fun getString(key: String?, defaultValue: String): String? {
        if (key == null) {
            throw IllegalArgumentException("Shared Pref key can't be NULL")
        }

        val sharedPref = getSystemSharedPreference()
        return if (sharedPref != null) sharedPref.getString(key, defaultValue) else defaultValue
    }

    fun getInteger(key: String): Int {
        return getInteger(key, -1)
    }

    fun getInteger(key: String?, defaultValue: Int): Int {
        if (key == null) {
            throw IllegalArgumentException("Shared Pref key can't be NULL")
        }

        val sharedPref = getSystemSharedPreference()
        return sharedPref?.getInt(key, defaultValue) ?: defaultValue
    }

    fun getBoolean(key: String): Boolean {
        return getBoolean(key, false)
    }

    fun getBoolean(key: String?, defaultValue: Boolean): Boolean {
        if (key == null) {
            throw IllegalArgumentException("Shared Pref key can't be NULL")
        }

        val sharedPref = getSystemSharedPreference()
        return sharedPref?.getBoolean(key, defaultValue) ?: defaultValue
    }
}
