package com.finalrevise.android.utils;

import android.app.DownloadManager;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.net.URISyntaxException;

public class Downloader implements DownloadReceiver.Listener {

    private Listener listener;
    public final DownloadManager downloadManager;

    private DownloadReceiver receiver = null;

    private long downloadId = -1;

    public static Downloader newInstance(Listener listener) {
        Context context = listener.getContext();
        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        return new Downloader(downloadManager, listener);
    }

    Downloader(DownloadManager downloadManager, Listener listener) {
        this.downloadManager = downloadManager;
        this.listener = listener;
    }

    public void download(String downloadUrl, String paperName, String folderName) {
        if (!isDownloading()) {
            register();
            File downloadDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            File destination = new File(downloadDir.getAbsolutePath() + "/" + folderName);
            destination.mkdirs();
            try {
                File sourceFile = new File(destination, paperName);

                Uri destinationUri = Uri.fromFile(sourceFile);
                Uri uri = Uri.parse(downloadUrl);
                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setTitle(folderName);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, uri.getLastPathSegment());
                request.setDescription("Downloading " + paperName);
                request.setDestinationUri(destinationUri);
                downloadId = downloadManager.enqueue(request);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isDownloading() {
        return downloadId >= 0;
    }

    void register() {
        if (receiver == null) {
            receiver = new DownloadReceiver(this);
            receiver.register(listener.getContext());
        }
    }

    @Override
    public void downloadComplete(long completedDownloadId) {
        if (downloadId == completedDownloadId) {
            DownloadManager.Query query = new DownloadManager.Query();
            query.setFilterById(downloadId);
            downloadId = -1;
            unregister();
            Cursor cursor = downloadManager.query(query);
            if (cursor == null || cursor.getCount() == 0) {
                listener.cancelFileDownloaded();
            } else
                while (cursor.moveToNext()) {
                    getFileInfo(cursor);
                }
            cursor.close();
        }
    }

    public void unregister() {
        if (receiver != null) {
            receiver.unregister(listener.getContext());
        }
        receiver = null;
    }

    private void getFileInfo(Cursor cursor) {
        int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
        if (status == DownloadManager.STATUS_SUCCESSFUL) {
            Long id = cursor.getLong(cursor.getColumnIndex(DownloadManager.COLUMN_ID));
            Uri uri = downloadManager.getUriForDownloadedFile(id);
            String mimeType = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_MEDIA_TYPE));
            try {
                String filePath = PathUtil.getPath(listener.getContext(), uri);
                listener.fileDownloaded(new File(filePath), mimeType, listener.getContext());
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        } else {
            listener.cancelFileDownloaded();
        }
    }

    public void cancel() {
        Log.e("Download", "" + isDownloading());
        if (isDownloading()) {
            downloadManager.remove(downloadId);
            downloadId = -1;
            unregister();
        }
    }

    public interface Listener {
        void fileDownloaded(File uri, String mimeType, Context context);

        void cancelFileDownloaded();

        Context getContext();
    }
}