package com.finalrevise.android.utils

import android.content.Context
import android.net.ConnectivityManager
import android.widget.Toast
import com.finalrevise.android.FinalReviseApplication
import com.finalrevise.android.network.ApiService
import io.reactivex.Scheduler

object NetworkUtil {

    fun getConnectivityStatus(context: Context): Int {
        val cm = context
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetwork = cm.activeNetworkInfo
        if (null != activeNetwork) {
            if (activeNetwork.type == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI

            if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE
        }
        return TYPE_NOT_CONNECTED
    }

    fun setConnectivityStatusString(context: Context): String? {
        val conn = getConnectivityStatus(context)
        var status: String? = null
        if (conn == NetworkUtil.TYPE_WIFI) {
            status = "Wifi enabled"
            isInternetConnected = true
        } else if (conn == NetworkUtil.TYPE_MOBILE) {
            status = "Mobile data enabled"
            isInternetConnected = true
        } else if (conn == NetworkUtil.TYPE_NOT_CONNECTED) {
            status = "Not connected to Internet"
            isInternetConnected = false
        }
        return status
    }

    fun isInternetConnected(context: Context): Boolean {
        try {
            setConnectivityStatusString(context)
        } catch (e: Exception) {

        }

        return isInternetConnected
    }

    private val TYPE_WIFI = 1
    private val TYPE_MOBILE = 2
    private val TYPE_NOT_CONNECTED = 0

    var isInternetConnected = false

    @JvmStatic
    fun getApiService(context: Context): ApiService {
        return getApplication(context).getApiService()
    }

    fun getApplication(context: Context) = FinalReviseApplication.application
        ?: context.applicationContext as FinalReviseApplication

    fun getScheduler(context: Context): Scheduler {
        return getApplication(context).subscribeScheduler()
    }

    fun showApiErrorToast(context: Context, it: Throwable){
        Toast.makeText(context, "Error", Toast.LENGTH_LONG).show()
    }
}
