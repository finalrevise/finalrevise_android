package com.finalrevise.android.utils

import java.util.HashMap
import java.util.LinkedHashMap

object ExpandableListDataPump {
    val data: Map<String, Map<String, Long>>
        get() {
            val expandableListDetail = LinkedHashMap<String, Map<String, Long>>()

            val universities = HashMap<String, Long>()
            universities["DU - DELHI UNIVERSITY"] = 1536821255850L
            universities["IP - INDRAPRASTHA UNIVERSITY"] = 1539457728597L
            universities["IGNOU"] = 1552200931079L

            val notes = HashMap<String, Long>()
            notes["DU NOTES"] = 1537623737625L
            notes["CBSE"] = 1568574281493L
            notes["MBA NOTES"] = 1568573260597L

            val entrance = HashMap<String, Long>()
            entrance["DU-JAT"] = 1537888298825L
            entrance["DU LLB MSc MA"] = 1537888435827L
            entrance["CAT"] = 1537816435098L
            entrance["GATE"] = 1537891463114L

            val gov = HashMap<String, Long>()
            gov["SSC CGL"] = 1537302429026L
            gov["CTET"] = 1537601136101L
            gov["DELHI POLICE"] = 1537302950818L
            gov["IBPS"] = 1537619116809L
            gov["GATE"] = 1537891463114L

            val books = HashMap<String, Long>()
            books["COMPUTER SCIENCE"] = 1561658659436L
            books["NCERT"] = 1569598018050L
            
            val empty = HashMap<String, Long>()

            expandableListDetail["Home"] = empty
            expandableListDetail["Search"] = empty
            expandableListDetail["Bookmarks"] = empty
            expandableListDetail["Downloads"] = empty
//            expandableListDetail["Top Universities"] = universities
//            expandableListDetail["Top Notes"] = notes
//            expandableListDetail["Top Entrance"] = entrance
//            expandableListDetail["Top Gov. Jobs"] = gov
//            expandableListDetail["Top Free Books"] = books
            expandableListDetail["Upload Notes/Papers"] = empty
            expandableListDetail["Study Abroad"] = empty
            expandableListDetail["Rate our work"] = empty
            expandableListDetail["About us"] = empty
            expandableListDetail["Contact us"] = empty
            expandableListDetail["Our Services"] = empty
            return expandableListDetail
        }
}