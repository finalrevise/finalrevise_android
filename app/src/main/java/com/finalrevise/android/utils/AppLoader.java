package com.finalrevise.android.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.Window;
import android.view.WindowManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import com.finalrevise.android.R;

/**
 * Created by nashish on 10/10/2017.
 */

public class AppLoader {
    private Dialog dialog;
    private Context token;
    private long delay;
    private boolean cancelable;
    private Handler handler;

    private AppLoader(Builder builder) {
        this.token = builder.token;
        this.delay = builder.delay;
        this.cancelable = builder.cancelable;
    }

    public void show() {
        if (token != null) {
            this.dialog = new Dialog(token);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(token, android.R.color.transparent));
            dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(cancelable);
            dialog.setContentView(R.layout.progress_loader);
            if (delay == 0) {
                dialog.show();
            } else {
                handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(runnable, delay);
            }
        } else {
            throw new IllegalArgumentException("Activity token is required. " +
                    "Please pass any Activity/Fragment instance as token only");
        }
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (dialog != null) {
                dialog.show();
            }
        }
    };

    public void hide() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        if(handler != null) {
            handler.removeCallbacks(runnable);
            handler = null;
        }
    }

    public static class Builder {
        private Context token;
        private long delay;
        private boolean cancelable = false;

        private Context getWindowToken(Object mToken) {
            Context mContext = null;
            if (mToken != null) {
                if (mToken instanceof AppCompatActivity) {
                    mContext = (AppCompatActivity) mToken;
                } else if (mToken instanceof Fragment) {
                    mContext = ((Fragment) mToken).getActivity();
                }
            }
            return mContext;
        }

        private Builder() {

        }

        public static Builder newInstance() {
            return new Builder();
        }

        public AppLoader.Builder withToken(Object token) {
            this.token = getWindowToken(token);
            return this;
        }

        public AppLoader.Builder withDelay(long delay) {
            this.delay = delay;
            return this;
        }

        public AppLoader.Builder isCancelable(boolean cancelable) {
            this.cancelable = cancelable;
            return this;
        }

        public AppLoader build() {
            return new AppLoader(this);
        }
    }
}
