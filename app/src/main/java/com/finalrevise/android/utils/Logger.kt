package com.finalrevise.android.utils

import android.text.TextUtils
import android.util.Log
import com.finalrevise.android.BuildConfig

/**
 * Created by nashish on 8/28/2017.
 */

object Logger {
    val ENABLE_LOGS = true
    val TAG = "Apnamba"
    var LEVEL = if (BuildConfig.DEBUG) Log.VERBOSE else Log.ERROR

    fun log(level: Int, msg: String) {

        if (!ENABLE_LOGS) {
            return
        }
        if (level < LEVEL) {
            return
        }
        if (TextUtils.isEmpty(msg)) {
            return
        }

        when (level) {
            Log.VERBOSE -> Log.v(Logger.TAG, msg)
            Log.DEBUG -> Log.d(Logger.TAG, msg)
            Log.INFO -> Log.i(Logger.TAG, msg)
            Log.WARN -> Log.w(Logger.TAG, msg)
            Log.ERROR -> Log.e(Logger.TAG, msg)
            Log.ASSERT -> Log.wtf(Logger.TAG, msg)
            else -> {
            }
        }
    }

    /**
     * Send a @DEBUG log message.
     *
     * @param msg The message you would like logged
     */
    fun d(msg: String) {
        Logger.log(Log.DEBUG, msg)
    }

    /**
     * Send a @VERBOSE log message.
     *
     * @param msg The message you would like logged
     */
    fun v(msg: String) {
        Logger.log(Log.VERBOSE, msg)
    }

    /**
     * Send an @INFO log message.
     *
     * @param msg The message you would like logged
     */
    fun i(msg: String) {
        Logger.log(Log.INFO, msg)
    }

    /**
     * Send a @WARN log message.
     *
     * @param msg The message you would like logged
     */
    fun w(msg: String) {
        Logger.log(Log.WARN, msg)
    }

    /**
     * Send an @ERROR log message.
     *
     * @param msg The message you would like logged
     */
    fun e(msg: String) {
        Logger.log(Log.ERROR, msg)
    }

    /**
     * Send a @WTF log message.
     *
     * @param msg The message you would like logged
     */
    fun wtf(msg: String) {
        Logger.log(Log.ASSERT, msg)
    }

    /**
     * Send a @DEBUG log message and log the exception.
     *
     * @param msg       The message you would like logged
     * @param throwable An exception to log
     */
    fun d(msg: String, throwable: Throwable) {
        Logger.d(msg + '\n'.toString() + Log.getStackTraceString(throwable))
    }

    /**
     * Send a @VERBOSE log message and log the exception.
     *
     * @param msg       The message you would like logged
     * @param throwable An exception to log
     */
    fun v(msg: String, throwable: Throwable) {
        Logger.v(msg + '\n'.toString() + Log.getStackTraceString(throwable))
    }

    /**
     * Send an @INFO log message and log the exception.
     *
     * @param msg       The message you would like logged
     * @param throwable An exception to log
     */
    fun i(msg: String, throwable: Throwable) {
        Logger.i(msg + '\n'.toString() + Log.getStackTraceString(throwable))
    }

    /**
     * Send a @WARN log message and log the exception.
     *
     * @param msg       The message you would like logged
     * @param throwable An exception to log
     */
    fun w(msg: String, throwable: Throwable) {
        Logger.w(msg + '\n'.toString() + Log.getStackTraceString(throwable))
    }

    /**
     * Send an @ERROR log message and log the exception.
     *
     * @param msg       The message you would like logged
     * @param throwable An exception to log
     */
    fun e(msg: String, throwable: Throwable) {
        Logger.e(msg + '\n'.toString() + Log.getStackTraceString(throwable))
    }

    /**
     * Send a @WTF log message and log the exception.
     *
     * @param msg       The message you would like logged
     * @param throwable An exception to log
     */
    fun wtf(msg: String, throwable: Throwable) {
        Logger.wtf(msg + '\n'.toString() + Log.getStackTraceString(throwable))
    }
}
