package com.finalrevise.android

import android.app.Application
import com.finalrevise.android.network.ApiFacade
import com.finalrevise.android.network.ApiService
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

class FinalReviseApplication : Application(){

    companion object{
        var application: FinalReviseApplication? = null
            private set
            get() = field ?: application
    }

    private var apiService: ApiService? = null
    private var scheduler: Scheduler? = null

    fun getInstance(): FinalReviseApplication?{
        if (application == null){
            synchronized(FinalReviseApplication::class.java){
                if(application == null){
                    application = FinalReviseApplication()
                }
            }
        }
        return application
    }

    override fun onCreate() {
        super.onCreate()
        application = this
    }

    fun getApiService() : ApiService {
        if(apiService == null){
            apiService =  ApiFacade().create()
        }
        return apiService as ApiService
    }

    fun subscribeScheduler() : Scheduler{
        if (scheduler == null)
            scheduler = Schedulers.io()
        return scheduler as Scheduler
    }

    override fun onTerminate() {
        super.onTerminate()
        application = null
    }
}