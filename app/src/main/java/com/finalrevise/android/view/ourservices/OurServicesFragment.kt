package com.finalrevise.android.view.ourservices

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.finalrevise.android.R
import com.finalrevise.android.base.BaseFragment
import com.google.android.gms.ads.AdView

class OurServicesFragment : BaseFragment(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setShowHamburgerMenu(true)
    }

    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_service, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mAdView = view.findViewById<View>(R.id.adView) as AdView
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<View>(R.id.contact_service).setOnClickListener(this)
    }


    override fun onClick(view: View) {
        val id = view.id
        when (id) {
            R.id.contact_service -> mainActivity?.let { whatsapp(it) }
        }
    }

    companion object {

        fun newInstance() =  OurServicesFragment()

        @SuppressLint("NewApi")
        fun whatsapp(activity: Activity) {
            try {
                val text = "You want our service regarding ...."// Replace with your message.

                val toNumber =
                    "919457822676" // Replace with mobile phone number without +Sign or leading zeros, but with country code
                //Suppose your country is India and your phone number is “xxxxxxxxxx”, then you need to send “91xxxxxxxxxx”.


                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse("http://api.whatsapp.com/send?phone=$toNumber&text=$text")
                activity.startActivity(intent)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    override fun getToolbarMenu(): Int {
        return 0
    }
}
