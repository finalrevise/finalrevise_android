package com.finalrevise.android.view.subcat

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.bumptech.glide.Glide
import com.finalrevise.android.R
import com.finalrevise.android.base.BaseFragment
import com.finalrevise.android.databinding.FragmentSubcatBinding
import com.finalrevise.android.interfaces.OnViewClickListener
import com.finalrevise.android.model.subcat.GroupedPageItemModel
import com.finalrevise.android.utils.GridItemDecoration
import com.finalrevise.android.utils.NetworkUtil
import com.finalrevise.android.view.material.MaterialFragment
import com.finalrevise.android.viewmodel.subcat.SubCatVM
import com.finalrevise.android.viewmodel.subcat.SubCatVMFactory
import com.google.android.gms.ads.AdView
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.app_toolbar.view.*

class SubCatFragment : BaseFragment(), OnViewClickListener {

    private lateinit var binding: FragmentSubcatBinding
    private var title: String = ""
    private var type: String = ""
    var result: ArrayList<GroupedPageItemModel> = ArrayList()

    private val viewModel: SubCatVM by activityViewModels {
        SubCatVMFactory(requireContext())
    }

    companion object {
        fun newInstance() = SubCatFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setShowHamburgerMenu(true)
        type = arguments?.getString("type")!!
        title = arguments?.getString("title")!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_subcat, container, false)
        binding.viewModel = viewModel
        return binding.root
    }

    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mAdView = view.findViewById<View>(R.id.adView) as AdView
        super.onViewCreated(view, savedInstanceState)
        binding.toolBar.toolBarTitle.text = title
        binding.subcatList.setHasFixedSize(true)
        val layout = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        binding.subcatList.addItemDecoration(GridItemDecoration(10, 2))
        binding.subcatList.layoutManager = layout
        binding.subcatList.adapter = SubCatAdapter(emptyList(), this)
        Glide.with(this).load(R.raw.loading_small).into(binding.finalreviseImage)
        NetworkUtil.getApiService(mainActivity!!).getSubCatList(type)
            .subscribeOn(NetworkUtil.getScheduler(mainActivity!!))
            .observeOn(AndroidSchedulers.mainThread())
            .map {
                it.getResult()!!.getResult()!!
            }
            .subscribe({
                binding.finalreviseImage.visibility = View.GONE
                binding.subcatList.visibility = View.VISIBLE
                for (key in it.keys) {
                    it[key]?.let { it1 -> result.addAll(it1) }
                }
                (binding.subcatList.adapter as SubCatAdapter).setData(result)
            }, {
                binding.finalreviseImage.visibility = View.GONE
                NetworkUtil.showApiErrorToast(mainActivity!!, it)
            })
    }

    override fun onClick(v: View?, position: Int) {
        var groupedPageItemModel = result[position]
        var bundle = Bundle()
        bundle.putString("type", groupedPageItemModel!!.getMetaTitle())
        bundle.putString("title", groupedPageItemModel!!.getPageItemName())
        if(groupedPageItemModel.getIsChild()!!){
            val subCatFragment = newInstance()
            subCatFragment.arguments = bundle
            replaceToBackStack(mainActivity?.supportFragmentManager!!, R.id.container, subCatFragment , false)
        }else{
            val materialFragment = MaterialFragment.newInstance()
            materialFragment.arguments = bundle
            replaceToBackStack(mainActivity?.supportFragmentManager!!, R.id.container, materialFragment , false)
        }
    }

    override fun getToolbarMenu(): Int {
        return 0
    }

}