package com.finalrevise.android.view.entity

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.finalrevise.android.R
import com.finalrevise.android.interfaces.OnViewClickListener
import com.finalrevise.android.model.entity.Data

class EntityAdapter (private var list: List<Data>, val itemClickListener: OnViewClickListener)
    : RecyclerView.Adapter<EntityViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EntityViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return EntityViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: EntityViewHolder, position: Int) {
        val data: Data = list[position]
        holder.bind(data, itemClickListener)
    }

    fun setList(list: List<Data>){
        this.list = list
        notifyDataSetChanged()
    }

}

class EntityViewHolder(inflater: LayoutInflater, var parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.entity_list_item, parent, false)) {

    private var mTitleView: TextView? = null
    private var entityImg: ImageView? = null


    init {
        mTitleView = itemView.findViewById(R.id.entity_title)
        entityImg = itemView.findViewById(R.id.entity_image)
    }

    fun bind(
        data: Data,
        itemClickListener: OnViewClickListener
    ) {
        mTitleView?.text = data.entityName
        entityImg?.let { Glide.with(parent.context).load(data.entityImage).into(it) }
        itemView.setOnClickListener {
            itemClickListener.onClick(itemView, adapterPosition)
        }
    }

}