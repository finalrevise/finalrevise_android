package com.finalrevise.android.view.entity

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.finalrevise.android.R
import com.finalrevise.android.base.BaseFragment
import com.finalrevise.android.databinding.FragmentEntityBinding
import com.finalrevise.android.interfaces.OnViewClickListener
import com.finalrevise.android.model.entity.Data
import com.finalrevise.android.utils.NetworkUtil
import com.finalrevise.android.view.subcat.SubCatFragment
import com.finalrevise.android.viewmodel.entity.EntityVM
import com.finalrevise.android.viewmodel.entity.EntityVMFactory
import com.google.android.gms.ads.AdView
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.app_toolbar.view.*

class EntityFragment : BaseFragment(), OnViewClickListener{

    private lateinit var binding: FragmentEntityBinding
    private var type: String = ""
    private var title: String = ""
    private lateinit var data: List<Data>

    private val viewModel: EntityVM by activityViewModels {
        EntityVMFactory(requireContext())
    }

    companion object {
        fun newInstance() = EntityFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setShowHamburgerMenu(true)
        type = arguments?.getString("type")!!
        title = arguments?.getString("title")!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_entity, container, false)
        binding.viewModel = viewModel
        return binding.root
    }

    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mAdView = view.findViewById<View>(R.id.adView) as AdView
        super.onViewCreated(view, savedInstanceState)
        binding.toolBar.toolBarTitle.text = title
        binding.entityList.setHasFixedSize(true)
        val layout  = LinearLayoutManager(mainActivity)
        binding.entityList.layoutManager = layout
        binding.entityList.adapter = EntityAdapter(ArrayList<Data>(), this)
        Glide.with(this).load(R.raw.loading_small).into(binding.finalreviseImage)
        NetworkUtil.getApiService(mainActivity!!).getEntityList(type)
            .subscribeOn(NetworkUtil.getScheduler(mainActivity!!))
            .observeOn(AndroidSchedulers.mainThread())
            .map {
                it.data!!
            }
            .subscribe({
                binding.finalreviseImage.visibility = View.GONE
                binding.entityList.visibility = View.VISIBLE
                binding.noInternet.visibility = View.GONE
                (binding.entityList.adapter as EntityAdapter).setList(it)
                data = it
            }, {
                binding.finalreviseImage.visibility = View.GONE
                binding.noInternet.visibility = View.VISIBLE
            })
    }

    override fun onClick(v: View?, index: Int) {
        var bundle = Bundle()
        bundle.putString("title", data[index].tag)
        bundle.putString("type", data[index].tag)
        val subCatFragment = SubCatFragment.newInstance()
        subCatFragment.arguments = bundle
        replaceToBackStack(mainActivity?.supportFragmentManager!!, R.id.container, subCatFragment , false)
    }

    override fun getToolbarMenu(): Int {
        return 0
    }

}