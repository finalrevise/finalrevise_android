package com.finalrevise.android.view.company

import android.annotation.SuppressLint
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.finalrevise.android.R
import com.finalrevise.android.base.BaseFragment
import com.google.android.gms.ads.AdView

class AboutFragment : BaseFragment(){

    private var about: TextView? = null
    private var title: TextView? = null

    companion object {
        fun newInstance() = AboutFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setShowHamburgerMenu(true)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_about, container, false)
    }

    override fun getToolbarMenu(): Int {
        return 0
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mAdView = view.findViewById<View>(R.id.adView) as AdView
        super.onViewCreated(view, savedInstanceState)
        about = view.findViewById(R.id.txt_about)
        title = view.findViewById<View>(R.id.txt_about_title) as TextView
        title!!.paintFlags = title!!.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        about!!.text =
            "FinalRevise is the website made to help the students while preparing for their exams. It's having variety of sections, covering almost all type of exams.\n" +
                    "\n" +
                    "We are constantly adding new papers on our platform. Still if you don't find any paper which you need, email us : admin@finalrevise.com\n" +
                    "\n" +
                    "We are always happy to help you. Stay connected. Happy to hear your suggestions.\n" +
                    "\n"
    }

}