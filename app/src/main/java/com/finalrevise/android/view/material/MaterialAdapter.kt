package com.finalrevise.android.view.material

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.finalrevise.android.R
import com.finalrevise.android.interfaces.OnViewClickListener
import com.finalrevise.android.model.material.LinkedPageItemModel
import java.util.*

class MaterialAdapter (private var list: List<LinkedPageItemModel>, private val itemClickListener: OnViewClickListener)
    : RecyclerView.Adapter<EntityViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EntityViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return EntityViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: EntityViewHolder, position: Int) {
        val data: LinkedPageItemModel = list[position]
        holder.bind(data, itemClickListener)
    }

    fun setList(list: ArrayList<LinkedPageItemModel>){
        this.list = list
        notifyDataSetChanged()
    }

}

class EntityViewHolder(inflater: LayoutInflater, var parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.material_list_item, parent, false)) {

    private var mTitleView: TextView? = null


    init {
        mTitleView = itemView.findViewById(R.id.material_title)
    }

    fun bind(
        data: LinkedPageItemModel,
        itemClickListener: OnViewClickListener
    ) {
        mTitleView?.text = data.pageItemName
        itemView.setOnClickListener {
            itemClickListener.onClick(itemView, adapterPosition)
        }
    }

}