package com.finalrevise.android.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.finalrevise.android.R
import com.finalrevise.android.base.BaseDrawerActivity
import com.finalrevise.android.base.BaseFragment
import com.finalrevise.android.base.BaseFragment.Companion.replaceToBackStack
import com.finalrevise.android.view.home.HomeFragment
import com.finalrevise.android.view.sidebar.SideMenuFragment

class MainDrawerActivity : BaseDrawerActivity(){

    companion object{
        fun startActivity(context: Context){
            val intent = Intent(context, MainDrawerActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val homeFragment = HomeFragment.newInstance()
        replaceToBackStack(supportFragmentManager ,R.id.container, homeFragment, false)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_with_drawer
    }

    override fun getDrawerFragment(): BaseFragment {
        return SideMenuFragment.newInstance()
    }
}