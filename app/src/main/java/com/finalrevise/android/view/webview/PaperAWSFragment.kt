package com.finalrevise.android.view.webview

import android.Manifest
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.finalrevise.android.R
import com.finalrevise.android.base.BaseFragment
import com.finalrevise.android.databinding.FragmentPaperAwsBinding
import com.finalrevise.android.utils.Downloader
import com.finalrevise.android.utils.Logger.TAG
import com.finalrevise.android.utils.SharedPrefManager
import com.google.android.gms.ads.AdView
import kotlinx.android.synthetic.main.app_toolbar.view.*
import net.frederico.showtipsview.ShowTipsBuilder
import net.frederico.showtipsview.ShowTipsView
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException


class PaperAWSFragment : BaseFragment(), Downloader.Listener {

    private val READ_WRITE_REQUEST_CODE: Int = 101
    private var titlePaper: TextView? = null
    private var webViewPaper: WebView? = null
    private var paperName: String? = null
    private var paperUrl: String? = null
    private lateinit var binding: FragmentPaperAwsBinding
    private var downloadId: String? = null
    private var folderName: String? = null
    private var downloader: Downloader? = null
    private lateinit var nDialog: ProgressDialog

    companion object {
        fun newInstance() = PaperAWSFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setShowHamburgerMenu(true)
        paperName = arguments?.getString("paperName")
        paperUrl = arguments?.getString("paperUrl")
        folderName = arguments?.getString("folderName")
        downloadId = paperName?.let { SharedPrefManager.getInstance()?.getString(it) }
        downloader = Downloader.newInstance(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_paper_aws, container, false)
        return binding.root
    }

    override fun getToolbarMenu(): Int {
        return 0
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mAdView = view.findViewById<View>(R.id.adView) as AdView
        super.onViewCreated(view, savedInstanceState)
        binding.toolBar.toolBarTitle.text = paperName
        titlePaper = view.findViewById(R.id.txt_title_paper)
        webViewPaper = view.findViewById(R.id.webview_paper)
        titlePaper!!.text = paperName
        Glide.with(this).load(R.raw.loading_small).into(binding.finalreviseImage)
        webViewPaper!!.settings.javaScriptEnabled = true
        webViewPaper!!.webViewClient = AppWebViewClients()
        webViewPaper!!.loadUrl("https://docs.google.com/gview?embedded=true&url=$paperUrl")
        webViewPaper!!.setDownloadListener { url, userAgent, contentDisposition, mimetype, contentLength ->
            Toast.makeText(mainActivity, R.string.wait_down, Toast.LENGTH_LONG).show()
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }
        binding.toolBar.download.visibility = View.VISIBLE


        if (TextUtils.isEmpty(downloadId)) {
            Glide.with(this).load(R.raw.download).into(binding.toolBar.download)
        } else {
            Glide.with(this).load(R.drawable.downloaded).into(binding.toolBar.download)
        }

        binding.toolBar.download.setOnClickListener {
            if (TextUtils.isEmpty(downloadId)) {
                if (setupPermissions()) {
                    if (downloader!!.isDownloading) {
                        Toast.makeText(context, "Downloading is in progress...", Toast.LENGTH_LONG)
                            .show()
                    } else
                        download()
                }
            } else {
                deleteDownloadedFile()
            }

            //ads

            var hit = SharedPrefManager.getInstance()!!.getInteger("totalHits")
            if (hit != 0 && hit % 2 == 0) {
                showInterstitial()
            }
            SharedPrefManager.getInstance()!!.putPreference("totalHits", ++hit)
        }
        nDialog = ProgressDialog(this.context)
        nDialog.setMessage("Downloading $paperName inside $folderName ...")
        nDialog.setTitle(folderName)
        nDialog.isIndeterminate = false
        nDialog.setCancelable(false)
        verifyStoragePermissions(mainActivity)
        var tempCount: Int? = SharedPrefManager.getInstance()?.getInteger("walk1")
        if (tempCount != null) {
            if (tempCount < 3) {
                val showtips: ShowTipsView = ShowTipsBuilder(activity)
                    .setTarget(requireView().findViewById(R.id.download))
                    .setTitle(getString(R.string.walkthrough_down_button))
                    .setDescription(getString(R.string.walkthrough_down_des))
                    .setDelay(100)
                    .setBackgroundAlpha(128).setButtonText(getString(R.string.got_it))
                    .setCloseButtonColor(resources.getColor(R.color.colorPrimaryDark))
                    .setCloseButtonTextColor(Color.WHITE)
                    .build()
                showtips.show(activity)
                SharedPrefManager.getInstance()?.putPreference("walk1", ++tempCount)
            }
        }
    }

    private fun download() {
        downloader?.download(paperUrl, paperName, folderName)
        nDialog.show()
    }


    @Throws(IOException::class)
    fun copyFile(src: File, dst: File) {
        val inChannel = FileInputStream(src).channel
        val outChannel = FileOutputStream(dst).channel
        try {
            inChannel!!.transferTo(0, inChannel.size(), outChannel)
        } finally {
            inChannel?.close()
            outChannel?.close()
            deleteFile(src)
            nDialog.dismiss()
            showAlertDialog(
                "Download completed. Please check $folderName inside Downloaded material section.",
                paperName!!
            )
        }
    }

    private fun deleteFile(file: File) {
        file.delete()
        if (file.exists()) {
            file.canonicalFile.delete()
            if (file.exists()) {
                this.requireContext().deleteFile(file.name)
            }
        }
    }

    inner class AppWebViewClients : WebViewClient() {

        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            // TODO Auto-generated method stub
            view.loadUrl(url)
            return true
        }

        override fun onPageFinished(view: WebView, url: String) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url)
            val handler = Handler()
            handler.postDelayed({
                binding.webviewPaper.visibility = View.VISIBLE
                binding.finalreviseImage.visibility = View.GONE
            }, 1000)
            if (view.title == "") view.reload()
        }
    }

    private fun setupPermissions(): Boolean {
        val permission = this.mainActivity?.let {
            checkSelfPermission(
                it,
                WRITE_EXTERNAL_STORAGE
            )
        }

        return if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Permission to save denied")
            makeRequest()
            false
        } else {
            true
        }
    }

    private fun makeRequest() {
        this.mainActivity?.let {
            ActivityCompat.requestPermissions(
                it,
                arrayOf(WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE),
                READ_WRITE_REQUEST_CODE
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            READ_WRITE_REQUEST_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "Permission has been denied by user")
                } else {
                    download()
                }
            }
        }
    }

    override fun onDestroyView() {
        downloader?.cancel()
        super.onDestroyView()
    }

    override fun onDestroy() {
        super.onDestroy()
        downloader?.unregister()
    }

    private fun downloadCompleted(src: File?, contxt: Context?) {
        var downloadsStr = SharedPrefManager.getInstance()!!.getString("downloads")
        if (TextUtils.isEmpty(downloadId)) {
            Glide.with(this).load(R.drawable.downloaded).into(binding.toolBar.download)
            downloadsStr = if (TextUtils.isEmpty(downloadsStr)) {
                paperName
            } else {
                "$downloadsStr,$paperName"
            }
            paperName?.let { it1 -> SharedPrefManager.getInstance()?.putPreference(it1, paperUrl) }
            downloadId = paperName
        }
        if (downloadsStr!!.startsWith(",")) {
            downloadsStr = downloadsStr.replaceFirst(",".toRegex(), "")
        } else if (downloadsStr.endsWith(",")) {
            downloadsStr = downloadsStr.substring(0, downloadsStr.length - 1)
        }
        SharedPrefManager.getInstance()!!.putPreference("downloads", downloadsStr)

        // transfer file from download to cache
        src?.let { copyFile(it, getCacheFile(contxt)) }
    }

    private fun deleteDownloadedFile() {
        var downloadsStr = SharedPrefManager.getInstance()!!.getString("downloads")
        Glide.with(this).load(R.raw.download).into(binding.toolBar.download)
        if (downloadsStr!!.contains(",")) {
            downloadsStr = downloadsStr.replace((",$paperName").toRegex(), "")
            downloadsStr = downloadsStr.replace(("$paperName,").toRegex(), "")
        } else
            downloadsStr = paperName?.toRegex()?.let { it1 -> downloadsStr!!.replace(it1, "") }
        SharedPrefManager.getInstance()!!.getSharedPreferenceEditor()!!.remove(paperName).commit()
        downloadId = null
        Toast.makeText(mainActivity, R.string.downloaded, Toast.LENGTH_LONG).show()
        if (downloadsStr!!.startsWith(",")) {
            downloadsStr = downloadsStr.replaceFirst(",".toRegex(), "")
        } else if (downloadsStr.endsWith(",")) {
            downloadsStr = downloadsStr.substring(0, downloadsStr.length - 1)
        }
        SharedPrefManager.getInstance()!!.putPreference("downloads", downloadsStr)
        deleteFile(getCacheFile(context))
    }

    private fun getCacheFile(contxt: Context?): File {
        var cacheDirPath = contxt?.cacheDir?.absolutePath
        var fileCache = File("$cacheDirPath/$folderName")
        fileCache.mkdirs()
        var fullFile = File(fileCache.absolutePath + "/" + paperName)
        return fullFile
    }

    override fun fileDownloaded(src: File?, mimeType: String?, contxt: Context?) {
        downloadCompleted(src, contxt)
    }

    override fun cancelFileDownloaded() {
        nDialog.dismiss()
    }

    // Storage Permissions
    private val REQUEST_EXTERNAL_STORAGE = 1
    private val PERMISSIONS_STORAGE = arrayOf<String>(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    fun verifyStoragePermissions(activity: Activity?) {
        // Check if we have write permission
        val permission =
            activity?.let {
                ActivityCompat.checkSelfPermission(
                    it,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
            }
        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            activity?.let {
                ActivityCompat.requestPermissions(
                    it,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
                )
            }
        }
    }

}