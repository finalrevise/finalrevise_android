package com.finalrevise.android.view.home

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.MotionEvent.ACTION_DOWN
import android.view.MotionEvent.ACTION_UP
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.viewpager.widget.ViewPager
import com.finalrevise.android.R
import com.finalrevise.android.base.BaseFragment
import com.finalrevise.android.databinding.FragmentHomeBinding
import com.finalrevise.android.view.downloaded.DownloadStaticFragment
import com.finalrevise.android.view.entity.EntityFragment
import com.finalrevise.android.view.ourservices.OurServicesFragment
import com.finalrevise.android.viewmodel.home.HomeVM
import com.google.android.gms.ads.AdView
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.uppar_slider_layout.view.*
import java.util.*


/**
 * Created by sgangpur on 12/6/2017.
 */

class HomeFragment : BaseFragment(), View.OnClickListener {

    internal var ImageListresults = intArrayOf(
        R.drawable.du,
        R.drawable.entrance_exams,
        R.drawable.govt_jobs,
        R.drawable.ncert,
        R.drawable.school_boards
    )
    private var viewPager: ViewPager? = null
    private var adapter: InformationSliderAdapter? = null
    private var currentPage = 0
    private var NUM_PAGES = 6
    private var dots: Array<TextView>? = null
    private var dotsCount: Int = 0
    private val arrayViewPagerPageType = arrayOf(
        "du-delhi-university-previous-year-papers",
        "all-competitive-exams-previous-year-papers",
        "government-jobs-previous-year-papers",
        "ncert-national-council-of-educational-research-and-training-books",
        "school-boards-previous-year-papers"
    )
    private val arrayViewPagerPageTitle = arrayOf(
        "Delhi University Previous Year Papers", "Entrance Previous Year Papers",
        "Government Jobs Previous Year Papers", "NCERT Books", "School Boards Previous Year Papers"
    )
    private val viewModel: HomeVM by activityViewModels()
    private lateinit var binding: FragmentHomeBinding

    var timer: Timer? = null
    val DELAY_MS: Long = 500//delay in milliseconds before task is to be executed
    val PERIOD_MS: Long = 3000 // time in milliseconds between successive task executions.

    companion object {

        fun newInstance(): HomeFragment {
            val homeFragment = HomeFragment()
            return homeFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setShowHamburgerMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentHomeBinding>(
            inflater,
            R.layout.fragment_home,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mAdView = view.findViewById<View>(R.id.adView) as AdView
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initView() {
        viewPager = binding.upperLayout.pager
        adapter = InformationSliderAdapter(this.mContext, ImageListresults)
        viewPager?.adapter = adapter
        val tabLayout = binding.upperLayout.tab_layout as TabLayout
        tabLayout.setupWithViewPager(viewPager, true)
        /*After setting the adapter use the timer */
        val handler = Handler()
        val Update = Runnable {
            if (currentPage == NUM_PAGES - 1) {
                currentPage = 0
            }
            viewPager?.setCurrentItem(currentPage++, true)
        }

        timer = Timer() // This will create a new Thread
        timer?.schedule(object : TimerTask() { // task to be scheduled
            override fun run() {
                handler.post(Update)
            }
        }, DELAY_MS, PERIOD_MS)
        binding.layPrevious.setOnClickListener(this)
        binding.layAssign.setOnClickListener(this)
        binding.layBooks.setOnClickListener(this)
        binding.layNote.setOnClickListener(this)
        binding.laySyllabus.setOnClickListener(this)
        binding.layOurServices.setOnClickListener(this)
        binding.downloadedMaterial.setOnClickListener(this)
        binding.studyAbroad.setOnClickListener(this)
        binding.facebookFinalrevise.setOnClickListener(this)
        binding.instagramFinalrevise.setOnClickListener(this)
        binding.youtubeFinalrevise.setOnClickListener(this)
        binding.linkedinFinalrevise.setOnClickListener(this)

        viewPager?.setOnTouchListener(object : View.OnTouchListener {
            var oldX = 0f
            var newX = 0f
            var sens = 5f

            override fun onTouch(v: View, event: MotionEvent): Boolean {
                when (event.action) {
                    ACTION_DOWN -> oldX = event.x

                    ACTION_UP -> {
                        newX = event.x
                        if (Math.abs(oldX - newX) < sens) {
                            openDetailFragment(
                                arrayViewPagerPageType[viewPager!!.currentItem],
                                arrayViewPagerPageTitle[viewPager!!.currentItem]
                            )
                            return true
                        }
                        oldX = 0f
                        newX = 0f
                    }
                }

                return false
            }
        })
    }


    override fun onClick(v: View?) {
        val bundle = Bundle()
        val entityFragment = EntityFragment.newInstance()
        when (v?.id) {
            R.id.lay_previous -> {
                openEntityFragment(
                    bundle,
                    entityFragment,
                    "Past-Papers",
                    getString(R.string.previous)
                )
            }
            R.id.lay_note -> {
                openEntityFragment(bundle, entityFragment, "Notes", getString(R.string.notes))
            }
            R.id.lay_books -> {
                openEntityFragment(bundle, entityFragment, "Free-Books", getString(R.string.books))
            }
            R.id.lay_assign -> {
                openEntityFragment(
                    bundle,
                    entityFragment,
                    "Assignments",
                    getString(R.string.assignment)
                )
            }
            R.id.lay_syllabus -> {
                openEntityFragment(bundle, entityFragment, "Syllabus", getString(R.string.syllabus))
            }
            R.id.lay_our_services -> {
                val ourServicesFragment = OurServicesFragment.newInstance()
                replaceToBackStack(
                    mainActivity?.supportFragmentManager!!,
                    R.id.container,
                    ourServicesFragment,
                    false
                )
            }
            R.id.downloaded_material -> {
                val downloadStaticFragment = DownloadStaticFragment.newInstance()
                replaceToBackStack(
                    mainActivity!!.supportFragmentManager,
                    R.id.container,
                    downloadStaticFragment,
                    false
                )
            }
            R.id.study_abroad -> {
                val i = Intent(Intent.ACTION_VIEW)
                i.data =
                    Uri.parse("https://docs.google.com/forms/d/e/1FAIpQLSfdnI9kLjQ6DrJ6sjYVHGR7qZlsZnJ-xWGPi0Q2xupLkqYFyQ/viewform")
                startActivity(i)
            }
            R.id.facebook_finalrevise -> mainActivity!!.startActivity(
                mainActivity?.packageManager?.let {
                    newFacebookIntent(
                        it,
                        "https://www.facebook.com/finalrevise"
                    )
                }
            )
            R.id.linkedin_finalrevise -> context?.let {
                openExternalLink(
                    it,
                    "https://www.linkedin.com/company/finalrevise"
                )
            }
            R.id.instagram_finalrevise -> context?.let {
                openExternalLink(
                    it,
                    "https://www.instagram.com/finalrevise"
                )
            }
            R.id.youtube_finalrevise -> context?.let {
                openExternalLink(
                    it,
                    "https://www.youtube.com/channel/UCODEmMV_ka_Xb5hKoTGOEHQ"
                )
            }
        }
    }

    private fun newFacebookIntent(pm: PackageManager, url: String): Intent {
        var uri = Uri.parse(url)
        try {
            val applicationInfo = pm.getApplicationInfo("com.facebook.katana", 0)
            if (applicationInfo.enabled) {
                // http://stackoverflow.com/a/24547437/1048340
                uri = Uri.parse("fb://facewebmodal/f?href=$url")
            }
        } catch (ignored: PackageManager.NameNotFoundException) {
        }

        return Intent(Intent.ACTION_VIEW, uri)
    }

    private fun openExternalLink(mainActivity: Context, link: String) {
        try {
            val myIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link))
            mainActivity.startActivity(myIntent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(
                mainActivity,
                "No application can handle this request." + " Please install a webbrowser",
                Toast.LENGTH_LONG
            ).show()
            e.printStackTrace()
        }

    }

    private fun openEntityFragment(
        bundle: Bundle,
        entityFragment: EntityFragment,
        type: String,
        title: String
    ) {
        bundle.putString("type", type)
        bundle.putString("title", title)
        entityFragment.arguments = bundle
        replaceToBackStack(
            mainActivity?.supportFragmentManager!!,
            R.id.container,
            entityFragment,
            false
        )
    }

    override fun getToolbarMenu(): Int {
        return 0
    }
}
