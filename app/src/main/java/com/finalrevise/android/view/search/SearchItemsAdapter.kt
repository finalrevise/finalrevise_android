package com.finalrevise.android.view.search

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.finalrevise.android.R
import com.finalrevise.android.interfaces.OnViewClickListener
import com.finalrevise.android.model.search.Result

class SearchItemsAdapter (var list: ArrayList<Result>, val itemClickListener: OnViewClickListener)
    : RecyclerView.Adapter<EntityViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EntityViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return EntityViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: EntityViewHolder, position: Int) {
        val data: Result = list[position]
        holder.bind(data, itemClickListener)
    }

    fun setList(list: List<Result>, isNewSearch : Boolean){
        if (isNewSearch)
            this.list.clear()
        else
            this.list.addAll(list)
        notifyDataSetChanged()
    }

}

class EntityViewHolder(inflater: LayoutInflater, var parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.adapter_detail_paper_list, parent, false)) {

    private var mTitleView: TextView? = null


    init {
        mTitleView = itemView.findViewById(R.id.txt_paper_name)
    }

    fun bind(
        data: Result,
        itemClickListener: OnViewClickListener
    ) {
        mTitleView?.text = data.title
        itemView.setOnClickListener {
            itemClickListener.onClick(itemView, adapterPosition)
        }
    }

}