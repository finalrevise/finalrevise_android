package com.finalrevise.android.view.bookmark

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.finalrevise.android.R
import com.finalrevise.android.interfaces.OnViewClickListener
import com.finalrevise.android.utils.SharedPrefManager

class BookmarkAdapter(private var dataList: List<String>, var onViewClickListener: OnViewClickListener) : RecyclerView.Adapter<EntityViewHolder>() {

    var sharedPrefManager = SharedPrefManager.getInstance()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EntityViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return EntityViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: EntityViewHolder, position: Int) {
        val data: String = dataList[position]
        holder.bind(data, onViewClickListener)
    }

    fun setDataList(list: List<String>?) {
        this.dataList = list!!
        notifyDataSetChanged()
    }

}

class EntityViewHolder(inflater: LayoutInflater, var parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.material_list_item, parent, false)) {

    private var mTitleView: TextView? = null


    init {
        mTitleView = itemView.findViewById(R.id.material_title)
    }

    fun bind(
        data: String,
        itemClickListener: OnViewClickListener
    ) {
        var str = data.replace("-"," ")
        mTitleView?.text = str.substring(0, 1).toUpperCase() + str.substring(1)
        itemView.tag = str.substring(0, 1).toUpperCase() + str.substring(1)
        itemView.setOnClickListener {
            itemClickListener.onClick(itemView, adapterPosition)
        }
    }

}