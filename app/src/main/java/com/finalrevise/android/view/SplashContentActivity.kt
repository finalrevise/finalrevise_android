package com.finalrevise.android.view

import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.finalrevise.android.R
import com.finalrevise.android.databinding.ActivitySplashBinding
import com.finalrevise.android.viewmodel.SplashVM
import com.finalrevise.android.viewmodel.SplashVMFactory
import com.bumptech.glide.Glide
import com.finalrevise.android.base.BaseContentActivity


class SplashContentActivity : BaseContentActivity(){

    override fun getLayoutId(): Int {
        return R.layout.activity_splash
    }

    private lateinit var binding: ActivitySplashBinding

    private val viewModel: SplashVM by viewModels {
        SplashVMFactory(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)
        binding.viewModel = viewModel
        Glide.with(this).load(R.raw.bg).into(binding.finalreviseBgImage)
        Glide.with(this).load(R.raw.loading_small).into(binding.finalreviseImage)

        viewModel.startNextActivity.observe( this, Observer {
            if(it) {
                MainDrawerActivity.startActivity(this)
                finish()
            }
        })
    }


}