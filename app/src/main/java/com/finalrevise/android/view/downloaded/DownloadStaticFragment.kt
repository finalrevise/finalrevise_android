package com.finalrevise.android.view.downloaded

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.finalrevise.android.R
import com.finalrevise.android.base.BaseFragment
import com.finalrevise.android.databinding.FragmentDownloadedStaticBinding
import com.finalrevise.android.interfaces.OnViewClickListener
import com.finalrevise.android.model.subcat.GroupedPageItemModel
import com.finalrevise.android.view.downloadedfiles.DownloadedFragment
import com.google.android.gms.ads.AdView

class DownloadStaticFragment : BaseFragment(), OnViewClickListener {

    override fun getToolbarMenu(): Int {
        return 0
    }

    private lateinit var binding: FragmentDownloadedStaticBinding
    var result: ArrayList<GroupedPageItemModel> = ArrayList()


    companion object {
        fun newInstance() = DownloadStaticFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setShowHamburgerMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_downloaded_static, container, false)
        return binding.root
    }

    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mAdView = view.findViewById<View>(R.id.adView) as AdView
        super.onViewCreated(view, savedInstanceState)
        binding.freeBooksCard.setOnClickListener(this)
        binding.freeNotesCard.setOnClickListener(this)
        binding.freePapersCard.setOnClickListener(this)
        binding.freeSyllabusCard.setOnClickListener(this)
        binding.freeAssignmentCard.setOnClickListener(this)
    }

    override fun onClick(v: View?, position: Int) {
        var bundle = Bundle()
        val downloadedFragment = DownloadedFragment.newInstance()
        when {
            v?.id == R.id.free_books_card -> bundle.putString("folderName", "Books")
            v?.id == R.id.free_notes_card -> bundle.putString("folderName", "Notes")
            v?.id == R.id.free_papers_card -> bundle.putString("folderName", "Papers")
            v?.id == R.id.free_assignment_card -> bundle.putString("folderName", "Assignment")
            else -> bundle.putString("folderName", "Syllabus")
        }
        downloadedFragment.arguments = bundle
        replaceToBackStack(mainActivity?.supportFragmentManager!!, R.id.container, downloadedFragment , false)
    }
}