package com.finalrevise.android.view.webview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.finalrevise.android.R
import com.finalrevise.android.base.BaseFragment
import com.finalrevise.android.databinding.FragmentPdfviewBinding
import com.google.android.gms.ads.AdView
import kotlinx.android.synthetic.main.app_toolbar.view.*
import kotlinx.android.synthetic.main.fragment_pdfview.*
import java.io.File


class DownloadedFileFragment : BaseFragment() {

    private var titlePaper: TextView? = null
    private var paperName: String? = null
    private var paperUrl: String? = null
    private lateinit var binding: FragmentPdfviewBinding

    companion object {
        fun newInstance() = DownloadedFileFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setShowHamburgerMenu(true)
        paperName = arguments?.getString("paperName")
        paperUrl = arguments?.getString("paperUrl")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pdfview, container, false)
        return binding.root
    }

    override fun getToolbarMenu(): Int {
        return 0
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mAdView = view.findViewById<View>(R.id.adView) as AdView
        super.onViewCreated(view, savedInstanceState)
        binding.toolBar.toolBarTitle.text = paperName
        pdfView.fromFile(File(paperUrl)).load()
    }

}