package com.finalrevise.android.view.upload;

import static android.app.Activity.RESULT_OK;

import android.content.ClipData;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.finalrevise.android.R;
import com.finalrevise.android.base.BaseFragment;
import com.finalrevise.android.utils.CommonUtils;
import com.google.android.gms.ads.AdView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import kotlin.jvm.JvmStatic;

/**
 * Created by sgangpur on 22/6/2019.
 */

public class UploadFragment extends BaseFragment implements View.OnClickListener {

    private TextView title;
    private static final int SELECT_PICTURE = 1;
    private List<Uri> listUri = new ArrayList<>();
    private TextView fileName;

    @JvmStatic
    public static UploadFragment newInstance() {
        UploadFragment aboutFragment = new UploadFragment();
        aboutFragment.setArguments(new Bundle());
        return aboutFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setShowHamburgerMenu(true);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_upload, container, false);
    }

    @Override
    public int getToolbarMenu() {
        return 0;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mAdView = (AdView) view.findViewById(R.id.adView);
        super.onViewCreated(view, savedInstanceState);
        title = (TextView)view.findViewById(R.id.txt_upload_title);
        fileName = (TextView)view.findViewById(R.id.file_names);
        title.setPaintFlags(title.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        view.findViewById(R.id.choose_btn).setOnClickListener(this);
        view.findViewById(R.id.upload_btn).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.choose_btn:
                Intent intent = new Intent();
                intent.setType("*/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Choose application"), SELECT_PICTURE);
                break;
            case R.id.upload_btn:
                   new LongOperation().execute();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                String fileNames = "";
                listUri.clear();
                ClipData clipData = data.getClipData();
                if(clipData != null){
                    for(int i= 0 ; i < clipData.getItemCount(); i++){
                        Uri selectedUri = clipData.getItemAt(i).getUri();
                        listUri.add(selectedUri);
                        fileNames = fileNames + getFileNames(selectedUri.toString(), selectedUri) + "\n";
                    }
                }else{
                    if(data.getData() != null){
                        listUri.add(data.getData());
                        fileNames = getFileNames(data.getData().toString(), data.getData());
                    }
                }
                fileName.setText(fileNames);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private String getFileNames(String uriString, Uri uri){
        File myFile = new File(uriString);
        String path = myFile.getAbsolutePath();
        String displayName = null;

        if (uriString.startsWith("content://")) {
            Cursor cursor = null;
            try {
                cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        } else if (uriString.startsWith("file://")) {
            displayName = myFile.getName();
        }

        return displayName;
    }


    private class LongOperation extends AsyncTask<Void, Void, Void> {

        private StringBuilder msges = new StringBuilder();

        @Override
        protected Void doInBackground(Void... params) {

            for(Uri selectedUri : listUri){
                uploadFile(selectedUri);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            hideBlockingLoader();
            fileName.setText(msges);
        }

        @Override
        protected void onPreExecute() {
            showBlockingLoader();
        }


        public String uploadFile(Uri sourceFileUri) {
            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost("http://finalrevise-1206660368.us-east-2.elb.amazonaws.com/finalrevise/file/uploadFile");

                File sourceFile = new File(CommonUtils.getPath(mainActivity, sourceFileUri));

                FileBody fileBody1 = new FileBody(sourceFile);

                MultipartEntity reqEntity = new MultipartEntity(
                        HttpMultipartMode.BROWSER_COMPATIBLE);
                reqEntity.addPart("file", fileBody1);

                httpPost.setEntity(reqEntity);

                HttpResponse response = httpClient.execute(httpPost);
                HttpEntity resEntity = response.getEntity();

                if (resEntity != null) {
                    final String responseStr = EntityUtils.toString(resEntity)
                            .trim();
                    msges.append(responseStr);
                    msges.append("\n");
                }

            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
