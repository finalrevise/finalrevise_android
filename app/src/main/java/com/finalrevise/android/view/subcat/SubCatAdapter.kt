package com.finalrevise.android.view.subcat

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.finalrevise.android.R
import com.finalrevise.android.interfaces.OnViewClickListener
import com.finalrevise.android.model.subcat.GroupedPageItemModel

class SubCatAdapter(private var result: List<GroupedPageItemModel>?, val itemClickListener: OnViewClickListener) :
    RecyclerView.Adapter<SubCatViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubCatViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return SubCatViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int {
        result?.let {
            return result!!.size
        }
        return 0
    }

    override fun onBindViewHolder(holder: SubCatViewHolder, position: Int) {
        var data = this!!.result!![position]
        data?.let { holder.bind(data, itemClickListener) }
    }


    fun setData(result: List<GroupedPageItemModel>) {
        this.result = result
        notifyDataSetChanged()
    }

}

class SubCatViewHolder(inflater: LayoutInflater, var parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.subcat_list_item, parent, false)) {

    private var textCap: TextView? = null
    private var subcatImage: ImageView? = null


    init {
        subcatImage = itemView.findViewById(R.id.subcat_image)
        textCap = itemView.findViewById(R.id.subcat_title)
    }

    fun bind(
        data: GroupedPageItemModel,
        itemClickListener: OnViewClickListener
    ) {
        textCap?.text = data.getPageItemName()
        subcatImage?.let { Glide.with(parent.context).load(data.getCoverUrl()).fallback(R.drawable.logosq_light).into(it) }
        itemView.setOnClickListener {
            itemClickListener.onClick(itemView, adapterPosition)
        }
    }

}