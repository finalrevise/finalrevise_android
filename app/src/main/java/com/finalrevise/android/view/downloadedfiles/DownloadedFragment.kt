package com.finalrevise.android.view.downloadedfiles

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.finalrevise.android.R
import com.finalrevise.android.base.BaseFragment
import com.finalrevise.android.databinding.FragmentMaterialListBinding
import com.finalrevise.android.interfaces.OnViewClickListener
import com.finalrevise.android.utils.SharedPrefManager
import com.finalrevise.android.view.webview.DownloadedFileFragment
import kotlinx.android.synthetic.main.app_toolbar.view.*
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException

class DownloadedFragment : BaseFragment(), OnViewClickListener {

    private lateinit var binding: FragmentMaterialListBinding
    private var folderName: String = ""
    private lateinit var data: Array<File>

    companion object {
        fun newInstance() = DownloadedFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setShowHamburgerMenu(true)
        folderName = arguments?.getString("folderName")!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_material_list, container, false)
        return binding.root
    }

    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolBar.toolBarTitle.text = folderName
        binding.detailPaperList.setHasFixedSize(true)
        val layout = mainActivity?.let { LinearLayoutManager(it) }
        binding.detailPaperList.layoutManager = layout
        binding.detailPaperList.adapter = DownloadedAdapter(emptyArray(), this)
        binding.finalreviseImage.visibility = View.GONE

        val cacheDirPath = mainActivity?.cacheDir?.absolutePath
        val fileCache = File("$cacheDirPath/$folderName")
        val src = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absolutePath + "/" + folderName)

        if(src != null && src.exists() && src.listFiles() != null) {
            for(file in src.listFiles()){
                copyFile(file, File("${fileCache.absolutePath}/${file.name}"))
            }
        }

        if(fileCache != null && fileCache.exists()) {
            data = fileCache.listFiles()
            binding.noResult.visibility = View.GONE
            binding.detailPaperList.visibility = View.VISIBLE
            (binding.detailPaperList.adapter as DownloadedAdapter).setList(data)
        }else{
            binding.noResult.visibility = View.VISIBLE
            binding.detailPaperList.visibility = View.GONE
        }
        view.findViewById<View>(R.id.whatsapp_share).visibility = View.GONE
        view.findViewById<View>(R.id.share_options).visibility = View.GONE
    }

    @Throws(IOException::class)
    fun copyFile(src: File, dst: File) {
        val inChannel = FileInputStream(src).channel
        val outChannel = FileOutputStream(dst).channel
        try {
            inChannel!!.transferTo(0, inChannel.size(), outChannel)
        } finally {
            inChannel?.close()
            outChannel?.close()
            deleteFile(src)
        }
    }

    private fun deleteFile(file: File) {
        file.delete()
        if (file.exists()) {
            file.canonicalFile.delete()
            if (file.exists()) {
                this.requireContext().deleteFile(file.name)
            }
        }
    }

    override fun onClick(v: View?, index: Int) {
        var hit = SharedPrefManager.getInstance()!!.getInteger("totalHits")
        if (hit != 0 && hit % 3 == 0) {
            showInterstitial()
        }
        var bundle = Bundle()
        Toast.makeText(mainActivity, getString(R.string.upload_txt), Toast.LENGTH_LONG).show()
        val linkedPageItemModel = data[index]
        val paperFragment = DownloadedFileFragment.newInstance()
        bundle.putString("paperName", linkedPageItemModel.name)
        bundle.putString("paperUrl", linkedPageItemModel.absolutePath)
        paperFragment.arguments = bundle
        replaceToBackStack(mainActivity?.supportFragmentManager!!, R.id.container, paperFragment, false)
        SharedPrefManager.getInstance()!!.putPreference("totalHits", ++hit)
    }

    override fun getToolbarMenu(): Int {
        return 0
    }

    private fun shareExam(shareBody: String) {
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.app_name))
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
        startActivity(Intent.createChooser(sharingIntent, resources.getString(R.string.app_name)))
    }

    private fun shareonWhatsApp(shareBody: String) {
        val whatsappIntent = Intent(Intent.ACTION_SEND)
        whatsappIntent.type = "text/plain"
        whatsappIntent.setPackage("com.whatsapp")
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
        try {
            mainActivity!!.startActivity(whatsappIntent)
        } catch (ex: android.content.ActivityNotFoundException) {
            shareExam(shareBody)
        }

    }

}