package com.finalrevise.android.view.bookmark

import android.graphics.Paint
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.finalrevise.android.R
import com.finalrevise.android.base.BaseFragment
import com.finalrevise.android.interfaces.OnViewClickListener
import com.finalrevise.android.utils.SharedPrefManager
import com.finalrevise.android.view.material.MaterialFragment
import com.google.android.gms.ads.AdView

class BookmarkFragment : BaseFragment(), OnViewClickListener {

    private var bookmarkAdapter: BookmarkAdapter? = null
    private var mRecyclerView: RecyclerView? = null
    private var titleDetail: TextView? = null
    private var bookmarksStr: String? = null
    private var bookmarksArr: List<String>? = null
    private var textDesc: TextView? = null

    companion object {
        fun newInstance() = BookmarkFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setShowHamburgerMenu(true)
        bookmarkAdapter = BookmarkAdapter(emptyList(),this)
        bookmarksStr = SharedPrefManager.getInstance()?.getString("bookmarks")
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_bookmark, container, false)
    }

    override fun getToolbarMenu(): Int {
        return 0
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mAdView = view.findViewById<View>(R.id.adView) as AdView
        super.onViewCreated(view, savedInstanceState)
        if (!TextUtils.isEmpty(bookmarksStr))
            bookmarksArr = bookmarksStr!!.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toList()
        mRecyclerView = view.findViewById(R.id.detail_paper_list)
        titleDetail = view.findViewById(R.id.txt_title_detail)
        titleDetail!!.setText(R.string.bookmark)
        titleDetail!!.paintFlags = titleDetail!!.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        textDesc = view.findViewById(R.id.txt_detail_desc)
        mRecyclerView!!.setHasFixedSize(true)
        val mLayoutManager = LinearLayoutManager(activity)
        mRecyclerView!!.layoutManager = mLayoutManager
        mRecyclerView!!.adapter = this.bookmarkAdapter
        if (bookmarksArr == null || bookmarksArr!!.isEmpty()) {
            textDesc!!.visibility = View.VISIBLE
            textDesc!!.setText(R.string.no_bookmark)
        } else {
            bookmarkAdapter!!.setDataList(bookmarksArr)
            textDesc!!.visibility = View.GONE
        }
    }

    override fun onClick(v: View?, type: Int) {
        val bundle = Bundle()
        val pageType = bookmarksArr?.get(type)
        val detailFragment = MaterialFragment.newInstance()
        bundle.putString("type", pageType)
        bundle.putString("title", v?.tag.toString())
        detailFragment.arguments = bundle
        replaceToBackStack(mainActivity?.supportFragmentManager!!, R.id.container, detailFragment, false)
    }
}
