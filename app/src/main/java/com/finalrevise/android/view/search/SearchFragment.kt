package com.finalrevise.android.view.search

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.finalrevise.android.R
import com.finalrevise.android.base.BaseFragment
import com.finalrevise.android.interfaces.OnViewClickListener
import com.finalrevise.android.utils.NetworkUtil
import com.finalrevise.android.view.material.MaterialFragment
import com.finalrevise.android.view.subcat.SubCatFragment
import com.google.android.gms.ads.AdView
import io.reactivex.android.schedulers.AndroidSchedulers

class SearchFragment : BaseFragment(), OnViewClickListener {

    private var mLayoutManager: LinearLayoutManager = LinearLayoutManager(activity)
    private var searchItemsAdapter: SearchItemsAdapter? = null
    private var mRecyclerView: RecyclerView? = null
    private var searchTxt: EditText? = null
    private val PAGE_SIZE = 15
    private var searchString: String? = null
    private var isSearching: Boolean = false
    private var mIsRequestInMotion: Boolean = false
    private var inputMethodManager: InputMethodManager? = null
    var pastVisiblesItems: Int = 0
    var visibleItemCount: Int = 0
    var totalItemCount: Int = 0
    var fetchingPage = 0
    var result: ArrayList<com.finalrevise.android.model.search.Result> = ArrayList()
    private lateinit var scrollListener: RecyclerView.OnScrollListener
    private val lastVisibleItemPosition: Int
        get() = mLayoutManager.findLastVisibleItemPosition()

    companion object {
        fun newInstance() = SearchFragment()
    }

    override fun getToolbarMenu(): Int {
        return 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setShowHamburgerMenu(true)
        searchItemsAdapter = SearchItemsAdapter(arrayListOf(), this)
        mIsRequestInMotion = true
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mAdView = view.findViewById<View>(R.id.adView) as AdView
        super.onViewCreated(view, savedInstanceState)
        searchTxt = view.findViewById(R.id.ed_search_paper)
        mRecyclerView = view.findViewById(R.id.search_list)
        mRecyclerView!!.setHasFixedSize(true)
        mRecyclerView!!.layoutManager = mLayoutManager
        mRecyclerView!!.adapter = searchItemsAdapter
        Glide.with(this).load(R.raw.loading_small).into(view.findViewById(R.id.finalrevise_image))

        setRecyclerViewScrollListener()

        searchTxt!!.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            inputMethodManager!!.hideSoftInputFromWindow(searchTxt!!.windowToken, 0)
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchItemsAdapter!!.setList(emptyList(), true)
                callServerForSearchList(0)
                return@OnEditorActionListener true
            }
            false
        })
        inputMethodManager = mainActivity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        inputMethodManager!!.toggleSoftInputFromWindow(
            searchTxt!!.windowToken,
            InputMethodManager.SHOW_FORCED, 0
        )
        searchTxt!!.requestFocus()
    }

    @SuppressLint("CheckResult")
    private fun callServerForSearchList(offset: Int) {
        if (!TextUtils.isEmpty(searchTxt!!.text.toString()) && !isSearching) {
            requireView().findViewById<ImageView>(R.id.finalrevise_image).visibility = View.VISIBLE
            isSearching = true
            searchString = searchTxt!!.text.toString()

            NetworkUtil.getApiService(mainActivity!!).getSearchList(searchString!!, offset.toString())
                .subscribeOn(NetworkUtil.getScheduler(mainActivity!!))
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    isSearching = false
                    requireView().findViewById<ImageView>(R.id.finalrevise_image).visibility = View.GONE
                    it.result!!
                }
                .subscribe({
                    mRecyclerView!!.visibility = View.VISIBLE
                    searchItemsAdapter?.setList(it, false)
                }) {
                    requireView().findViewById<ImageView>(R.id.finalrevise_image).visibility = View.GONE
                    NetworkUtil.showApiErrorToast(mainActivity!!, it)
                }
        }
    }

    override fun onClick(v: View?, position: Int) {
        var groupedPageItemModel = searchItemsAdapter?.list!![position]
        var bundle = Bundle()
        bundle.putString("type", groupedPageItemModel.metaTitle)
        bundle.putString("title", groupedPageItemModel.title)
        if (groupedPageItemModel.isChild == 1) {
            val subCatFragment = SubCatFragment.newInstance()
            subCatFragment.arguments = bundle
            replaceToBackStack(mainActivity?.supportFragmentManager!!, R.id.container, subCatFragment, false)
        } else {
            val materialFragment = MaterialFragment.newInstance()
            materialFragment.arguments = bundle
            replaceToBackStack(mainActivity?.supportFragmentManager!!, R.id.container, materialFragment, false)
        }
    }

    private fun setRecyclerViewScrollListener() {
        scrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val totalItemCount = recyclerView!!.layoutManager?.itemCount
                val fetchPage = (totalItemCount?.div(PAGE_SIZE))?.plus(1)
                if (fetchingPage !== fetchPage) {
                    if (fetchPage != null) {
                        fetchingPage = fetchPage
                    }
                    callServerForSearchList(fetchPage!!)
                }
            }
        }
        mRecyclerView!!.addOnScrollListener(scrollListener)
    }

}