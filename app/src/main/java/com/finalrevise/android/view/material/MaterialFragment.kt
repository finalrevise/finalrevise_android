package com.finalrevise.android.view.material

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Html
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.finalrevise.android.R
import com.finalrevise.android.base.BaseFragment
import com.finalrevise.android.databinding.FragmentMaterialListBinding
import com.finalrevise.android.interfaces.OnViewClickListener
import com.finalrevise.android.model.material.LinkedPageItemModel
import com.finalrevise.android.utils.NetworkUtil
import com.finalrevise.android.utils.SharedPrefManager
import com.finalrevise.android.view.webview.PaperAWSFragment
import com.finalrevise.android.view.webview.PaperFragment
import com.finalrevise.android.viewmodel.material.MaterialVM
import com.finalrevise.android.viewmodel.material.MaterialVMFactory
import com.google.android.gms.ads.AdView
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.app_toolbar.view.*
import net.frederico.showtipsview.ShowTipsBuilder
import net.frederico.showtipsview.ShowTipsView

class MaterialFragment : BaseFragment(), OnViewClickListener {

    private var folderName: String = ""
    private var titleStr: String = ""
    private var navStr: String = ""
    private lateinit var binding: FragmentMaterialListBinding
    private var title: String = ""
    private var type: String = ""
    private var data: List<LinkedPageItemModel>? = null
    private var bookmarkId: String? = null

    private val viewModel: MaterialVM by activityViewModels {
        MaterialVMFactory(requireContext())
    }

    companion object {
        fun newInstance() = MaterialFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setShowHamburgerMenu(true)
        title = arguments?.getString("title")!!
        type = arguments?.getString("type")!!
        bookmarkId = SharedPrefManager.getInstance()?.getString(type)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_material_list, container, false)
        return binding.root
    }

    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mAdView = view.findViewById<View>(R.id.adView) as AdView
        super.onViewCreated(view, savedInstanceState)
        binding.toolBar.toolBarTitle.text = title
        binding.detailPaperList.setHasFixedSize(true)
        val layout = mainActivity?.let { LinearLayoutManager(it) }
        binding.detailPaperList.layoutManager = layout
        binding.detailPaperList.adapter = MaterialAdapter(ArrayList(), this)
        Glide.with(this).load(R.raw.loading_small).into(binding.finalreviseImage)
        binding.toolBar.bookmark.visibility = View.VISIBLE
        binding.toolBar.bookmark.setOnClickListener(this)
        if (TextUtils.isEmpty(bookmarkId)) {
            binding.toolBar.bookmark.setImageResource(R.drawable.icons_bookmark_100)
        } else {
            binding.toolBar.bookmark.setImageResource(R.drawable.icons8_bookmark_100)
        }
        binding.toolBar.bookmark.setOnClickListener(View.OnClickListener {
            var bookmarksStr = SharedPrefManager.getInstance()!!.getString("bookmarks")
            if (TextUtils.isEmpty(bookmarkId)) {
                binding.toolBar.bookmark.setImageResource(R.drawable.icons8_bookmark_100)
                if (TextUtils.isEmpty(bookmarksStr)) {
                    bookmarksStr = type
                } else {
                    bookmarksStr = "$bookmarksStr,$type"
                }
                SharedPrefManager.getInstance()?.putPreference(type, titleStr)
                bookmarkId = type
                Toast.makeText(mainActivity, R.string.added_bookmark, Toast.LENGTH_LONG).show()
            } else {
                binding.toolBar.bookmark.setImageResource(R.drawable.icons_bookmark_100)
                if (bookmarksStr!!.contains(",")) {
                    bookmarksStr = bookmarksStr.replace((",$type").toRegex(), "")
                    bookmarksStr = bookmarksStr.replace(("$type,").toRegex(), "")
                } else
                    bookmarksStr = bookmarksStr.replace(type.toRegex(), "")
                SharedPrefManager.getInstance()!!.getSharedPreferenceEditor()!!.remove(type).commit()
                bookmarkId = null
                Toast.makeText(mainActivity, R.string.remove_bookmark, Toast.LENGTH_LONG).show()
            }
            if (bookmarksStr.startsWith(",")) {
                bookmarksStr = bookmarksStr.replaceFirst(",".toRegex(), "")
            } else if (bookmarksStr.endsWith(",")) {
                bookmarksStr = bookmarksStr.substring(0, bookmarksStr.length - 1)
            }
            SharedPrefManager.getInstance()!!.putPreference("bookmarks", bookmarksStr)
        })

        NetworkUtil.getApiService(mainActivity!!).getMaterialList(type)
            .subscribeOn(NetworkUtil.getScheduler(mainActivity!!))
            .observeOn(AndroidSchedulers.mainThread())
            .map {
                it.result
            }
            .subscribe({
                it?.let { it ->
                    binding.finalreviseImage.visibility = View.GONE
                    binding.detailPaperList.visibility = View.VISIBLE
                    binding.shareLay.visibility = View.VISIBLE
                    (binding.detailPaperList.adapter as MaterialAdapter).setList(it.linkedPageItems?: emptyList<LinkedPageItemModel>() as java.util.ArrayList<LinkedPageItemModel>)
                    data = it.linkedPageItems
                    navStr = it.pagePath?: ""
                    titleStr = it.pageTitle?: ""
                    it.tag?.let {tag->
                        folderName = when {
                            tag.contains("Papers") -> {
                                "Papers"
                            }
                            tag.contains("Books") -> {
                                "Books"
                            }
                            tag.contains("Notes") -> {
                                "Notes"
                            }
                            else -> {
                                "Syllabus"
                            }
                        }
                    }
                }
            }, {
                binding.finalreviseImage.visibility = View.GONE
                NetworkUtil.showApiErrorToast(mainActivity!!, it)
            })

        view.findViewById<View>(R.id.whatsapp_share).setOnClickListener {
            shareonWhatsApp(
                "Go to -> " + navStr + " " + Html.fromHtml(
                    "<br>" + "<p> -via " + mainActivity!!.getString(R.string.app_name) + "</p>" + " https://play.google.com/store/apps/details?id=com.finalrevise.android"
                )
            )
        }
        view.findViewById<View>(R.id.share_options).setOnClickListener {
            shareExam(
                "Go to -> " + navStr + " " + Html.fromHtml(
                    "<br>" + "<p> -via " + mainActivity!!.getString(R.string.app_name) + "</p>" + " https://play.google.com/store/apps/details?id=com.finalrevise.android"
                )
            )
        }
        var tempCount: Int? = SharedPrefManager.getInstance()?.getInteger("walk")
        if (tempCount != null) {
            if (tempCount < 3) {
                val showtips: ShowTipsView = ShowTipsBuilder(activity)
                    .setTarget(requireView().findViewById(R.id.bookmark))
                    .setTitle(getString(R.string.walkthrough_bookmark_button))
                    .setDescription(getString(R.string.walkthrough_bookmark_des))
                    .setDelay(100)
                    .setBackgroundAlpha(128).setButtonText(getString(R.string.got_it))
                    .setCloseButtonColor(resources.getColor(R.color.colorPrimaryDark))
                    .setCloseButtonTextColor(Color.WHITE)
                    .build()
                showtips.show(activity)
                SharedPrefManager.getInstance()?.putPreference("walk", ++tempCount)
            }
        }
    }

    override fun onClick(v: View?, index: Int) {
        var hit = SharedPrefManager.getInstance()!!.getInteger("totalHits")
        if (hit != 0 && hit % 3 == 0) {
            showInterstitial()
        }
        var bundle = Bundle()
        Toast.makeText(mainActivity, getString(R.string.upload_txt), Toast.LENGTH_LONG).show()
        val linkedPageItemModel = data?.get(index)
        val paperUrl = linkedPageItemModel?.pageDocLink
        val paperFragment : BaseFragment = if(paperUrl?.contains("open") == true || paperUrl?.contains("view") == true || paperUrl?.contains("olders") == true)
            PaperFragment.newInstance()
        else
            PaperAWSFragment.newInstance()
        bundle.putString("paperName", linkedPageItemModel?.pageItemName)
        bundle.putString("paperUrl", linkedPageItemModel?.pageDocLink)
        bundle.putString("folderName", folderName)
        paperFragment.arguments = bundle
        replaceToBackStack(mainActivity?.supportFragmentManager!!, R.id.container, paperFragment, false)
        SharedPrefManager.getInstance()!!.putPreference("totalHits", ++hit)
    }

    override fun getToolbarMenu(): Int {
        return 0
    }

    private fun shareExam(shareBody: String) {
        var hit = SharedPrefManager.getInstance()!!.getInteger("totalHits")
        if (hit != 0 && hit % 3 == 0) {
            showInterstitial()
        }
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.app_name))
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
        startActivity(Intent.createChooser(sharingIntent, resources.getString(R.string.app_name)))
        SharedPrefManager.getInstance()!!.putPreference("totalHits", ++hit)
    }

    private fun shareonWhatsApp(shareBody: String) {
        var hit = SharedPrefManager.getInstance()!!.getInteger("totalHits")
        if (hit != 0 && hit % 3 == 0) {
            showInterstitial()
        }
        val whatsappIntent = Intent(Intent.ACTION_SEND)
        whatsappIntent.type = "text/plain"
        whatsappIntent.setPackage("com.whatsapp")
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
        try {
            mainActivity!!.startActivity(whatsappIntent)
        } catch (ex: android.content.ActivityNotFoundException) {
            shareExam(shareBody)
        }
        SharedPrefManager.getInstance()!!.putPreference("totalHits", ++hit)
    }

}