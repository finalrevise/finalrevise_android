package com.finalrevise.android.view.sidebar

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListAdapter
import android.widget.ExpandableListView
import androidx.core.app.ActivityCompat
import com.finalrevise.android.R
import com.finalrevise.android.base.BaseDrawerActivity
import com.finalrevise.android.base.BaseFragment
import com.finalrevise.android.utils.ExpandableListDataPump
import com.finalrevise.android.view.bookmark.BookmarkFragment
import com.finalrevise.android.view.company.AboutFragment
import com.finalrevise.android.view.company.ContactFragment
import com.finalrevise.android.view.downloaded.DownloadStaticFragment
import com.finalrevise.android.view.home.HomeFragment
import com.finalrevise.android.view.ourservices.OurServicesFragment
import com.finalrevise.android.view.search.SearchFragment
import com.finalrevise.android.view.upload.UploadFragment
import android.text.method.LinkMovementMethod
import android.widget.TextView
import java.util.*

class SideMenuFragment : BaseFragment() {

    private val REQUEST_CODE: Int = 101
    private var expandableListView: ExpandableListView? = null
    private var expandableListAdapter: ExpandableListAdapter? = null
    private var expandableListTitle: List<String>? = null
    private var expandableListDetail: Map<String, Map<String, Long>>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setShowNavigationIcon(false)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_side_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        expandableListView = view.findViewById<View>(R.id.side_menu_list) as ExpandableListView
        expandableListDetail = ExpandableListDataPump.data
        expandableListTitle = ArrayList(expandableListDetail!!.keys)
        expandableListAdapter = SideMenuListAdapter(
                view.context,
                expandableListTitle as ArrayList<String>, expandableListDetail!!
        )
        expandableListView!!.setAdapter(expandableListAdapter)
        expandableListView!!.setOnGroupExpandListener { groupPosition -> openFragmentFromDrawer(groupPosition, 0) }

        expandableListView!!.setOnGroupCollapseListener { groupPosition -> openFragmentFromDrawer(groupPosition, 0) }

        expandableListView!!.setOnChildClickListener { _, _, groupPosition, childPosition, _ ->
            val title = expandableListAdapter!!.getGroup(groupPosition) as String
            if (title.contains("Entrance") || title.contains("Gov") || title.contains("Int")) {
//                val detailFragment = DetailFragment.newInstance()
//                bundle.putLong("pageType", pageType)
//                detailFragment.setArguments(bundle)
//                BaseFragment.replaceToBackStack(MainDrawerActivity, detailFragment)
            } else {
//                val detailListFragment = DetailListFragment.newInstance()
//                bundle.putLong("pageType", pageType)
//                detailListFragment.setArguments(bundle)
//                BaseFragment.replaceToBackStack(MainDrawerActivity, detailListFragment)
            }
//            (MainDrawerActivity as MainDrawerActivity).closeDrawerOnItemClick()
            false
        }
        view.findViewById<TextView>(R.id.nav_privacyPolicy).apply {
            movementMethod = LinkMovementMethod.getInstance()
        }
    }

    private fun openFragmentFromDrawer(groupPos: Int, childPos: Int) {
//        if (groupPos != 4 && groupPos != 5 && groupPos != 6 && groupPos != 7 && groupPos != 8) {
            (mainActivity as BaseDrawerActivity).closeDrawerOnItemClick()
            val bundle = Bundle()
            when (groupPos) {
                0 -> {
                    val homeFragment = HomeFragment.newInstance()
                    replaceToBackStack(mainActivity!!.supportFragmentManager, R.id.container ,homeFragment, false)
                }
                1 -> {
                    val searchFragment = SearchFragment.newInstance()
                    replaceToBackStack(mainActivity!!.supportFragmentManager, R.id.container ,searchFragment, false)
                }
                2 -> {
                    val bookmarkFragment = BookmarkFragment.newInstance()
                    replaceToBackStack(mainActivity!!.supportFragmentManager, R.id.container ,bookmarkFragment, false)
                }
                3 -> {
                    val downloadStaticFragment = DownloadStaticFragment.newInstance()
                    replaceToBackStack(mainActivity!!.supportFragmentManager, R.id.container ,downloadStaticFragment, false)
                }
                4 -> {
                    checkExternalReadPermission()
                    val uploadFragment = UploadFragment.newInstance()
                    replaceToBackStack(mainActivity!!.supportFragmentManager, R.id.container ,uploadFragment, false)
                }
                5 -> {
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse("https://docs.google.com/forms/d/e/1FAIpQLSfdnI9kLjQ6DrJ6sjYVHGR7qZlsZnJ-xWGPi0Q2xupLkqYFyQ/viewform")
                    startActivity(i)
                }
                6 -> {
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse("https://play.google.com/store/apps/details?id=com.finalrevise.android")
                    startActivity(i)
                }
                7 -> {
                    val aboutFragment = AboutFragment.newInstance()
                    replaceToBackStack(mainActivity!!.supportFragmentManager, R.id.container ,aboutFragment, false)
                }
                8 -> { val contactFragment = ContactFragment.newInstance()
                    replaceToBackStack(mainActivity!!.supportFragmentManager, R.id.container ,contactFragment, false)
                }
                9 -> {
                    val ourServicesFragment = OurServicesFragment.newInstance()
                    replaceToBackStack(mainActivity!!.supportFragmentManager, R.id.container ,ourServicesFragment, false)
                }
                else -> {
                }
//            }
        }
    }

    @SuppressLint("NewApi")
    internal fun checkExternalReadPermission() {
        if (mainActivity?.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED && mainActivity?.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (shouldShowRequestPermissionRationale(
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                // Explain to the user why we need to read the contacts
            }

            this.mainActivity?.let {
                ActivityCompat.requestPermissions(
                    it,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    REQUEST_CODE
                )
            }

            return
        }
    }

    companion object {

        fun newInstance(): SideMenuFragment {
            val sideMenuFragment = SideMenuFragment()
            sideMenuFragment.arguments = Bundle()
            return sideMenuFragment
        }
    }

    override fun getToolbarMenu(): Int {
        return 0
    }
}