package com.finalrevise.android.view.company

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Paint
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.finalrevise.android.R
import com.finalrevise.android.base.BaseFragment
import com.google.android.gms.ads.AdView

class ContactFragment : BaseFragment(), View.OnClickListener {

    private var contact: TextView? = null
    private var title: TextView? = null
    private var followTxt: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setShowHamburgerMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_contact, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mAdView = view.findViewById<View>(R.id.adView) as AdView
        super.onViewCreated(view, savedInstanceState)
        contact = view.findViewById(R.id.txt_contact)
        title = view.findViewById<View>(R.id.txt_contact_title) as TextView
        followTxt = view.findViewById<View>(R.id.follow_us_text) as TextView
        view.findViewById<View>(R.id.facebook_finalrevise).setOnClickListener(this)
        view.findViewById<View>(R.id.instagram_finalrevise).setOnClickListener(this)
        view.findViewById<View>(R.id.youtube_finalrevise).setOnClickListener(this)
        view.findViewById<View>(R.id.linkedin_finalrevise).setOnClickListener(this)
        followTxt!!.paintFlags = followTxt!!.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        title!!.paintFlags = title!!.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        contact!!.text = "Website : http://finalrevise.com\n" + "Email :admin@finalrevise.com\n"
    }


    override fun onClick(view: View) {
        val id = view.id
        when (id) {
            R.id.facebook_finalrevise -> mainActivity!!.startActivity(
                mainActivity?.getPackageManager()?.let {
                    newFacebookIntent(
                        it,
                        "https://www.facebook.com/finalrevise"
                    )
                }
            )
            R.id.linkedin_finalrevise -> context?.let {
                openExternalLink(
                    it,
                    "https://www.linkedin.com/company/finalrevise"
                )
            }
            R.id.instagram_finalrevise -> context?.let {
                openExternalLink(
                    it,
                    "https://www.instagram.com/finalrevise"
                )
            }
            R.id.youtube_finalrevise -> context?.let {
                openExternalLink(
                    it,
                    "https://www.youtube.com/channel/UCODEmMV_ka_Xb5hKoTGOEHQ"
                )
            }
        }
    }

    companion object {
        fun newInstance() = ContactFragment()
    }

    fun openExternalLink(mainActivity: Context, link: String) {
        try {
            val myIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link))
            mainActivity.startActivity(myIntent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(
                mainActivity,
                "No application can handle this request." + " Please install a webbrowser",
                Toast.LENGTH_LONG
            ).show()
            e.printStackTrace()
        }

    }

    fun newFacebookIntent(pm: PackageManager, url: String): Intent {
        var uri = Uri.parse(url)
        try {
            val applicationInfo = pm.getApplicationInfo("com.facebook.katana", 0)
            if (applicationInfo.enabled) {
                // http://stackoverflow.com/a/24547437/1048340
                uri = Uri.parse("fb://facewebmodal/f?href=$url")
            }
        } catch (ignored: PackageManager.NameNotFoundException) {
        }

        return Intent(Intent.ACTION_VIEW, uri)
    }

    override fun getToolbarMenu(): Int {
        return 0
    }
}