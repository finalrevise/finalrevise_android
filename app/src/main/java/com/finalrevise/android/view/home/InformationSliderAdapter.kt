package com.finalrevise.android.view.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.viewpager.widget.PagerAdapter
import com.finalrevise.android.R

class InformationSliderAdapter(// Declare Variables
    internal var context: Context, internal var imageListresults: IntArray
) : PagerAdapter() {

    override fun getCount(): Int {
        return imageListresults.size
    }


    internal lateinit var inflater: LayoutInflater
    private var image: ImageView? = null

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val itemView = inflater.inflate(R.layout.adapter_dashboard_pager, container, false)
        image = itemView.findViewById(R.id.ivzoom)
        image!!.setBackgroundResource(imageListresults[position])
        container.addView(itemView)
        return itemView


    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        // Remove viewpager_item.xml from ViewPager
        container.removeView(`object` as LinearLayout)
    }
}
