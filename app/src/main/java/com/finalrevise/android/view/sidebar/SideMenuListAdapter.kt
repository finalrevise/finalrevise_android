package com.finalrevise.android.view.sidebar

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.TextView
import com.finalrevise.android.R

class SideMenuListAdapter(
    private val context: Context, private val expandableListTitle: List<String>,
    private val expandableListDetail: Map<String, Map<String, Long>>
) : BaseExpandableListAdapter() {

    override fun getChild(listPosition: Int, expandedListPosition: Int): String? {
        return this.expandableListDetail[this.expandableListTitle[listPosition]]?.keys?.toTypedArray()?.get(expandedListPosition)
    }

    override fun getChildId(listPosition: Int, expandedListPosition: Int): Long {
        return expandedListPosition.toLong()
    }

    override fun getChildView(
        listPosition: Int, expandedListPosition: Int,
        isLastChild: Boolean, convertView: View?, parent: ViewGroup
    ): View {
        var convertView = convertView
        val expandedListText = getChild(listPosition, expandedListPosition) as String
        if (convertView == null) {
            val layoutInflater = this.context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.list_item, null)
        }
        val expandedListTextView = convertView!!
            .findViewById<View>(R.id.expandedListItem) as TextView
        expandedListTextView.text = expandedListText
        return convertView
    }

    override fun getChildrenCount(listPosition: Int): Int {
        return if (this.expandableListDetail[this.expandableListTitle[listPosition]] != null)
            this.expandableListDetail[this.expandableListTitle[listPosition]]?.size!!
        else
            0
    }

    override fun getGroup(listPosition: Int): Any {
        return this.expandableListTitle[listPosition]
    }

    override fun getGroupCount(): Int {
        return this.expandableListTitle.size
    }

    override fun getGroupId(listPosition: Int): Long {
        return listPosition.toLong()
    }

    override fun getGroupView(
        listPosition: Int, isExpanded: Boolean,
        convertView: View?, parent: ViewGroup
    ): View {
        var convertView = convertView
        val listTitle = getGroup(listPosition) as String
        if (convertView == null) {
            val layoutInflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.list_group, null)
        }
        val listTitleTextView = convertView!!
            .findViewById<View>(R.id.listTitle) as TextView
        //        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.text = listTitle
        val indEx = convertView.findViewById<View>(R.id.explist_indicator)
        if (indEx != null) {
            val indicator = indEx as ImageView
            if (getChildrenCount(listPosition) == 0) {
                indicator.visibility = View.INVISIBLE
            } else {
                indicator.visibility = View.VISIBLE
                val stateSetIndex = if (isExpanded) 1 else 0
                val drawable = indicator.drawable
                drawable.state = GROUP_STATE_SETS[stateSetIndex]
            }
        }
        val ind = convertView.findViewById<ImageView>(R.id.menu_item_icon)
        ind.visibility = View.VISIBLE
        when (listPosition) {
            0 -> ind.setImageDrawable(context.getDrawable(R.drawable.ic_home))
            1 -> ind.setImageDrawable(context.getDrawable(R.drawable.ic_search))
            2 -> ind.setImageDrawable(context.getDrawable(R.drawable.ic_bookmark))
            3 -> ind.setImageDrawable(context.getDrawable(R.drawable.ic_download))
//            4 -> ind.setImageDrawable(context.getDrawable(R.drawable.ic_university))
//            5 -> ind.setImageDrawable(context.getDrawable(R.drawable.ic_study))
//            6 -> ind.setImageDrawable(context.getDrawable(R.drawable.ic_entrance))
//            7 -> ind.setImageDrawable(context.getDrawable(R.drawable.ic_gov))
//            8 -> ind.setImageDrawable(context.getDrawable(R.drawable.ic_book))
            4 -> ind.setImageDrawable(context.getDrawable(R.drawable.ic_uploading))
            5 -> ind.setImageDrawable(context.getDrawable(R.drawable.ic_abroad))
            6 -> ind.setImageDrawable(context.getDrawable(R.drawable.ic_rating))
            7 -> ind.setImageDrawable(context.getDrawable(R.drawable.ic_about))
            8 -> ind.setImageDrawable(context.getDrawable(R.drawable.ic_contact))
            9 -> ind.setImageDrawable(context.getDrawable(R.drawable.ic_services))
        }
        return convertView
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(listPosition: Int, expandedListPosition: Int): Boolean {
        return true
    }

    companion object {
        private val EMPTY_STATE_SET = intArrayOf()
        private val GROUP_EXPANDED_STATE_SET = intArrayOf(android.R.attr.state_expanded)
        private val GROUP_STATE_SETS = arrayOf(
            EMPTY_STATE_SET, // 0
            GROUP_EXPANDED_STATE_SET // 1
        )
    }
}