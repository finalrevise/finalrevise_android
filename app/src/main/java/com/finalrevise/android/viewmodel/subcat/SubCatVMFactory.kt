package com.finalrevise.android.viewmodel.subcat

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class SubCatVMFactory(private val context: Context) : ViewModelProvider.NewInstanceFactory(){


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SubCatVM(context) as T
    }

}