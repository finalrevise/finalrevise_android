package com.finalrevise.android.viewmodel.subcat

import android.content.Context
import android.view.View
import androidx.databinding.ObservableInt
import androidx.lifecycle.ViewModel
import com.finalrevise.android.model.entity.Entity
import com.finalrevise.android.utils.CommonUtils
import com.finalrevise.android.utils.NetworkUtil
import com.finalrevise.android.viewmodel.common.LayoutProgressVM
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

class SubCatVM(private val context : Context) : ViewModel(){

    val progressVM = LayoutProgressVM()
    val mainLayoutVis = ObservableInt(View.VISIBLE)
    private val compositeDisposable: CompositeDisposable? = CompositeDisposable()

    companion object {
        const val DELAY_TIME = 4000L
    }

    fun getEntityList(title: String): Observable<Entity>? {
        showHideProgress(true)
        if(CommonUtils.isNetworkAvailable(context)){
//            val disposable =

                return NetworkUtil.getApiService(context).getEntityList(title)
//                .subscribeOn(NetworkUtil.getScheduler(context))
//                .observeOn(AndroidSchedulers.mainThread())
//                .map {
//                    it.data!!
//                }
//                .subscribe({
//                    showHideProgress(false)
//
//                }, {
//                    showHideProgress(false)
//                    NetworkUtil.showApiErrorToast(context, it)
//                })
//            compositeDisposable?.add(disposable)
        }else
            return null
    }

    private fun showHideProgress(isShow : Boolean){
        if(isShow){
            progressVM.visibility.set(View.VISIBLE)
            mainLayoutVis.set(View.GONE)
        }else{
            progressVM.visibility.set(View.GONE)
            mainLayoutVis.set(View.VISIBLE)
        }
    }

}