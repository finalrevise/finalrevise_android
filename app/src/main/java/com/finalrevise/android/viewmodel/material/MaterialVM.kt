package com.finalrevise.android.viewmodel.material

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.finalrevise.android.model.material.Result
import com.finalrevise.android.utils.NetworkUtil
import io.reactivex.android.schedulers.AndroidSchedulers


class MaterialVM(private val context: Context) : ViewModel() {

    private var mutableLiveData: MutableLiveData<Result>? = null

//    @SuppressLint("CheckResult")
//    fun getMaterialList(title: String) {
//        NetworkUtil.getApiService(context).getMaterialList(title)
//            .subscribeOn(NetworkUtil.getScheduler(context))
//            .observeOn(AndroidSchedulers.mainThread())
//            .map {
//                it.result
//            }
//            .subscribe({
//                mutableLiveData = it
//            }, {
//                NetworkUtil.showApiErrorToast(context, it)
//            })
//    }

    fun getResult(): MutableLiveData<Result>? {
        return mutableLiveData
    }
}