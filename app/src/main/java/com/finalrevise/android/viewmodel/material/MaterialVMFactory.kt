package com.finalrevise.android.viewmodel.material

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MaterialVMFactory(private val context: Context) : ViewModelProvider.NewInstanceFactory(){


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MaterialVM(context) as T
    }

}