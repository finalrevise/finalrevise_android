package com.finalrevise.android.viewmodel.entity

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class EntityVMFactory(private val context: Context) : ViewModelProvider.NewInstanceFactory(){


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return EntityVM(context) as T
    }

}