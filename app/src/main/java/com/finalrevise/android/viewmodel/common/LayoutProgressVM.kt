package com.finalrevise.android.viewmodel.common

import android.view.View
import androidx.annotation.Keep
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableInt

@Keep
class LayoutProgressVM {

    var visibility = ObservableInt(View.GONE)
    val clickableFocusable = ObservableBoolean(true)
}