package com.finalrevise.android.viewmodel

import android.content.Context
import android.os.Handler
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SplashVM(private val context : Context) : ViewModel(){

    companion object {
        const val DELAY_TIME = 4000L
    }

    val appVersionError =  ObservableField<String>()
    var startNextActivity = MutableLiveData<Boolean>(false)

    init {
        startDelay()
    }

    private fun startDelay(){
        Handler().postDelayed({
            startNextActivity.apply {
                value = true
            }
        },
            DELAY_TIME)
    }

}