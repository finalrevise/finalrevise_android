package com.finalrevise.android.network

import com.finalrevise.android.BuildConfig
import com.finalrevise.android.utils.ApiConstants
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit

class ApiFacade {

    companion object{
        const val  TIME_OUT = 60L

        fun getGson(serializeNull: Boolean = false): Gson{
            val builder = GsonBuilder().setDateFormat("dd-mm-yyyy")
            if(serializeNull)
                builder.serializeNulls()
            return builder.create()
        }

    }

    fun create(): ApiService {
        val interceptor = BDInterceptor()
        interceptor.level = LoggingInterceptor.Level.BODY

        val logging = HttpLoggingInterceptor()
        logging.level = if (BuildConfig.DEBUG)
            HttpLoggingInterceptor.Level.BODY
        else HttpLoggingInterceptor.Level.NONE

        val okClient = OkHttpClient.Builder()
            .addInterceptor(logging)
            .addInterceptor(interceptor)
            .retryOnConnectionFailure(true)
            .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(TIME_OUT, TimeUnit.SECONDS)
            .build()
        val retrofit = Retrofit.Builder().baseUrl(ApiConstants.BASE_URL)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(getGson()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okClient)
            .build()
        return retrofit.create(ApiService::class.java)
    }

    class BDInterceptor : LoggingInterceptor() {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            val original = chain.request()
            val requestBuilder = original.newBuilder()
                .header("Content-Type", "application/json")
                .method(original.method, original.body)

            var isLoggedIn = false
//            FinalReviseApplication.application?.let {
//                isLoggedIn = com.camtrack.tracker.utils.BaseUtils.isLoggedIn(it)
//                if (isLoggedIn)
//                    requestBuilder.addHeader(ApiConstants.FIREBASE_HEADER_KEY, getAccessToken(it))
//            }

            val modifiedRequest = requestBuilder.build()
            val response = chain.proceed(modifiedRequest)

            val unauthorized =
                isLoggedIn && response.code == 401/*Constants.ResponseCode.HTTP_UNAUTHORIZED*/

            var count = 0
            var refreshedToken = ""

            return  response
        }
    }
}