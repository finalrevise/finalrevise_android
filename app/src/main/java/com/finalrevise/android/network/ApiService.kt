package com.finalrevise.android.network

import com.finalrevise.android.model.entity.Entity
import com.finalrevise.android.model.material.DetailResponseModel
import com.finalrevise.android.model.search.SearchListResponseModel
import com.finalrevise.android.model.subcat.SubCat
import io.reactivex.Observable
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.apache.http.entity.mime.MultipartEntity
import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.*


interface ApiService {

    @GET("/finalrevise/v1/entity/{title}")
    fun getEntityList(@Path("title") title: String): Observable<Entity>

    @GET("/finalrevise/v1/subcat/{title}")
    fun getSubCatList(@Path("title") title: String): Observable<SubCat>

    @GET("/finalrevise/v1/content/{title}")
    fun getMaterialList(@Path("title") title: String): Observable<DetailResponseModel>

    @GET("/finalrevise/v1/search/{title}/10/{offset}/0")
    fun getSearchList(
        @Path("title") title: String,
        @Path("offset") offset: String
    ): Observable<SearchListResponseModel>

    @GET
    @Streaming
    fun downloadMaterial(@Url fileUrl: String?): Call<ResponseBody?>?

    @Multipart
    @POST("/finalrevise/file/uploadFile")
    fun uploadFile(
        @Part("file") file: MultipartEntity?
    ): Observable<String>?

}