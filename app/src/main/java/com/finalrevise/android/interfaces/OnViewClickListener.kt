package com.finalrevise.android.interfaces

import android.view.View

interface OnViewClickListener : View.OnClickListener {

    override fun onClick(v: View?) {
        onClick(v, 0)
    }

    fun onClick(v: View?, type: Int = 0)

}