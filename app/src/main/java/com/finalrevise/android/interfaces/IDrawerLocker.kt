package com.finalrevise.android.interfaces

interface IDrawerLocker {
    fun setDrawerLocked(shouldLock: Boolean)
}